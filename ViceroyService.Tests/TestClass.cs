﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ViceroyService.Tests
{
    [TestFixture]
    public class TestClass
    {
        [Test]
        public async Task TestGetAllFiles()
        {
            Synchronizer sync = new Synchronizer();

            string[] files = await sync.GetAllFiles("*.*", "include", true);

            Assert.Greater(files.Length, 1500000);
        }

        [Test]
        public void TestGetClientDirectories()
        {
            Synchronizer sync = new Synchronizer();

            string[] clients = sync.GetClientsForTest();

            Assert.NotZero(clients.Length);
        }

        [Test]
        public void TestGetMatterDirectories()
        {
            Synchronizer sync = new Synchronizer();

            string[] matters = sync.GetAllMatterDirectories();

            Assert.NotZero(matters.Length);
        }

        [Test]
        public async Task TestMapMattersAsync()
        {
            Synchronizer sync = new Synchronizer();

            Dictionary<int, string> map = await sync.MatchInfolinxMattersToMatterDirectories("%");

            Assert.Greater(map.Count, 300000);
        }

        [Test]
        public void TestGetConnectionStringFromConfigFile()
        {
            Main main = new Main();

            string connectionString = main.GetConnectionStringFromConfigFile();

            Assert.NotNull(connectionString);
        }
    }
}
