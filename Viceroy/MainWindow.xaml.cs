﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Viceroy
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public const string FILE_SHARE_REPOSITORY_TYPE_NAME = "File Share";

        private ApplicationSettingsWindow _applicationSettingsWindow;
        private DatabaseSettingsWindow _databaseSettingsWindow;
        private FileSettingsWindow _fileSettingsWindow;
        private WebServiceSettingsWindow _webServiceSettingsWindow;

        private DatabaseServices _databaseServices;
        private Logger _logger;

        private DataView _connectionsView;



        public bool ShowFilesSettingsWindow { get; set; }

        public bool DBConnectionValidated { get; set; }



        public MainWindow()
        {
            InitializeComponent();

            _logger = new Logger("Viceroy");
        }



        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            WindowLoadSetup();
        }

        public void WindowLoadSetup()
        {
            Cursor = Cursors.Arrow;

            if (ShowFilesSettingsWindow)
            {
                if (MessageBox.Show("The path for the log file is invalid" + Environment.NewLine + "Please enter a valid path.", "Configure File Settings", MessageBoxButton.OK, MessageBoxImage.Error) == MessageBoxResult.OK)
                {
                    OpenFileSettingsWindow();
                    return;
                }
            }
            else
            {
                _databaseServices = new DatabaseServices(Properties.Settings.Default.ConnectionString);

                if (!this.DBConnectionValidated)
                {
                    MessageBox.Show("Validating connection to the database.", "Configure Database Settings", MessageBoxButton.OK, MessageBoxImage.Information);
                    Cursor = Cursors.Wait;
                    this.DBConnectionValidated = DatabaseServices.TestConnection();
                }

                try
                {
                    if (this.DBConnectionValidated)
                    {
                        BindGrid();
                    }
                    else
                    {
                        if (MessageBox.Show("There was an error connecting to the database!" + Environment.NewLine + "Please re-enter database credentials.", "Configure Database Settings", MessageBoxButton.OK, MessageBoxImage.Error) == MessageBoxResult.OK)
                        {
                            Cursor = Cursors.Arrow;
                            OpenDatabaseSettingsWindow();
                            return;
                        }
                    }

                    RepositoryTypeComboBox.SelectedIndex = 0;

                    Cursor = Cursors.Arrow;
                }
                catch (Exception ex)
                {
                    if (MessageBox.Show("There was an error connecting to the database!" + Environment.NewLine + ex.Message + Environment.NewLine + "Please re-enter database credentials.", "Configure Database Settings", MessageBoxButton.OK, MessageBoxImage.Error) == MessageBoxResult.OK)
                    {
                        Cursor = Cursors.Arrow;
                        OpenDatabaseSettingsWindow();
                        return;
                    }
                }
            }

            SaveSettingsToDatabase();
        }

        private void BindGrid()
        {
            DataTable dt = _databaseServices.GetConnections();
            _connectionsView = dt.DefaultView;
            grdConnections.ItemsSource = _connectionsView;
        }

        private void SaveSettingsToDatabase()
        {
            DatabaseServices DS = null;
            string PropValue = "";
            string PropName = "";
            string PropType = "";

            try
            {
                DS = new DatabaseServices(Properties.Settings.Default.ConnectionString);

                DataTable dt = DS.GetVRSettings();

                if (dt.Rows.Count == 0)
                {
                    foreach (SettingsProperty currentProperty in Properties.Settings.Default.Properties)
                    {
                        PropValue = Properties.Settings.Default[currentProperty.Name].ToString();
                        PropName = currentProperty.Name;
                        PropType = currentProperty.PropertyType.ToString();
                        DS.InsertVRSettings(currentProperty.Name, currentProperty.PropertyType.ToString(), "User", PropValue);
                    }
                }
                else
                {
                    foreach (SettingsProperty currentProperty in Properties.Settings.Default.Properties)
                    {
                        bool blnFound = false;
                        foreach (DataRow row in dt.Rows)
                        {                                                   
                            PropValue = Properties.Settings.Default[currentProperty.Name].ToString();
                            PropName = currentProperty.Name;
                            PropType = currentProperty.PropertyType.ToString();

                            if (row["SETTING_NAME"].ToString() == currentProperty.Name)
                            {
                                blnFound = true;
                                if (row["SETTING_VALUE"].ToString() != PropValue)
                                {
                                    DS.UpdateVRSettings(currentProperty.Name, currentProperty.PropertyType.ToString(), "User", PropValue);
                                }
                            }
                        }
                        if (!blnFound)
                        {
                            DS.InsertVRSettings(PropName, PropType, "User", PropValue);
                        }
                    }
                }
            }
            catch
            {
                // Hopefully do something someday maybe...
            }
        }



        private void RepositoryTypeComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            LoadConnectionTypeListItems();
        }

        private void LoadConnectionTypeListItems()
        {
            string[] connectionTypes =
            {
                "All",
                FILE_SHARE_REPOSITORY_TYPE_NAME
            };

            List<ComboBoxItem> items = new List<ComboBoxItem>();
            foreach (var repositoryType in connectionTypes)
            {
                items.Add(new ComboBoxItem()
                {
                    Content = repositoryType
                });
            }

            RepositoryTypeComboBox.ItemsSource = items;
        }



        private void AddRepositoryButton_Click(object sender, RoutedEventArgs e)
        {
            string connectionType = GetSelectedConnectionType();

            if (connectionType == FILE_SHARE_REPOSITORY_TYPE_NAME)
            {
                Window window = new FileShareConnectionWindow();
                window.Owner = this;
                window.Title = "Add " + window.Title;
                window.Show();
            }
        }

        private string GetSelectedConnectionType()
        {
            string selectedConnectionType = "";

            if (RepositoryTypeComboBox.SelectedItem != null)
            {
                selectedConnectionType = ((ComboBoxItem)RepositoryTypeComboBox.SelectedItem).Content.ToString();
            }

            return selectedConnectionType;
        }



        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            object o = grdConnections.SelectedItem;
            if (o != null)
            {
                DataRowView row = o as DataRowView;
                switch (row["CONNECTION_TYPE"].ToString())
                {
                    case "File Share":
                        FileShareConnectionWindow window = new FileShareConnectionWindow();
                        window.ConnectionID = Convert.ToInt32(row["ID"]);
                        window.NetworkPath = row["PATH"].ToString();
                        window.Filter = row["FILTER"].ToString();
                        window.IndexContent = Convert.ToBoolean(row["INDEX_CONTENT"]);
                        window.IncludeSubdirectories = Convert.ToBoolean(row["INCLUDE_SUB_DIRECTORIES"]);
                        window.Archive = Convert.ToBoolean(row["ARCHIVE"]);
                        window.ConnectionName = row["CONNECTION_NAME"].ToString();
                        window.FilterInclude = row["FILTER_INCLUDE"].ToString();
                        window.SearchPattern = row["SEARCH_PATTERN"] != DBNull.Value ? row["SEARCH_PATTERN"].ToString() : "";
                        window.UpdateConnection = true;
                        window.Owner = this;
                        window.Title = "Edit " + window.Title;
                        window.Show();
                        break;
                }
            }
        }



        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            object o = grdConnections.SelectedItem;
            if (o != null)
            {
                try
                {
                    if (MessageBox.Show("Do you wish to delete the selected connection?", "View Connections", MessageBoxButton.YesNoCancel, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        DatabaseServices ds = new DatabaseServices(Properties.Settings.Default.ConnectionString);

                        if (o != null)
                        {
                            DataRowView row = o as DataRowView;
                            ds.DeleteConnection(Convert.ToInt32(row["ID"]));
                            BindGrid();
                        }
                    }
                }
                catch
                {
                    MessageBox.Show("No connection was selected" + Environment.NewLine +
                        "Please select an existing connection to remove.", "View Connections", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("No connection was selected" + Environment.NewLine +
                       "Please select an existing connection to remove.", "View Connections", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }



        private void btnTest_Click(object sender, RoutedEventArgs e)
        {
            object o = grdConnections.SelectedItem;

            if (o != null)
            {
                DataRowView row = o as DataRowView;

                switch (row["CONNECTION_TYPE"].ToString())
                {
                    case "File Share":
                        ValidateFileShareConnection(row);
                        break;
                }
            }
        }

        protected bool ValidateFileShareConnection(DataRowView row)
        {
            bool blnRet = true;

            try
            {
                string[] aryFilters = row["FILTER"].ToString().Split(',');
                foreach (string filter in aryFilters)
                {
                    //they should start with an *
                    if (!filter.StartsWith("*"))
                    {
                        MessageBox.Show("There is an error in the filter that was entered!" + Environment.NewLine + "Please enter a valid filter." + Environment.NewLine +
                           "Filters should start with an asterick followed by a period, followed by the desired extension." + Environment.NewLine +
                           "Additional extensions should be entered preceded by a comma!", "Configure File Share Connection", MessageBoxButton.OK, MessageBoxImage.Error);
                        return false;
                    }
                    else
                    {
                        try
                        {
                            if (filter.Substring(1, 1) != ".")
                            {
                                MessageBox.Show("There is an error in the filter that was entered!" + Environment.NewLine + "Please enter a valid filter." + Environment.NewLine +
                                "Filters should start with an asterick followed by a period, followed by the desired extension." + Environment.NewLine +
                                "Additional extensions should be entered preceded by a comma!", "Configure File Share Connection", MessageBoxButton.OK, MessageBoxImage.Error);
                                return false;
                            }
                        }
                        catch
                        {
                            MessageBox.Show("There is an error in the filter that was entered!" + Environment.NewLine + "Please enter a valid filter." + Environment.NewLine +
                               "Filters should start with an asterick followed by a period, followed by the desired extension." + Environment.NewLine +
                               "Additional extensions should be entered preceded by a comma!", "Configure File Share Connection", MessageBoxButton.OK, MessageBoxImage.Error);
                            return false;
                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show("There is an error in the filter that was entered!" + Environment.NewLine + "Please enter a valid filter." + Environment.NewLine +
                            "Filters should start with an asterick followed by a period, followed by the desired extension." + Environment.NewLine +
                            "Additional extensions should be entered preceded by a comma!", "Configure File Share Connection", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            //verify 
            if (Directory.Exists(row["PATH"].ToString()))
            {
                MessageBox.Show("Connection successful!", "Configure File Share Connection", MessageBoxButton.OK, MessageBoxImage.Information);
                blnRet = true;
            }
            else
            {
                MessageBox.Show("Connection not successful!" + Environment.NewLine + "Please enter a valid path.", "Configure File Share Connection", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            return blnRet;
        }



        private void RepositoryTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (RepositoryTypeComboBox.SelectedItem != null && ((ComboBoxItem)RepositoryTypeComboBox.SelectedItem).Content.ToString() != "All")
            {
                if (DBConnectionValidated)
                {
                    BindGridForSelectedConnectionType();
                }

                AddRepositoryButton.IsEnabled = true;
            }
            else //get all connections
            {
                if (DBConnectionValidated)
                {
                    BindGrid();
                }

                AddRepositoryButton.IsEnabled = false;
            }
        }

        public void BindGridForSelectedConnectionType()
        {
            string connectionType = ((ComboBoxItem)RepositoryTypeComboBox.SelectedItem).Content.ToString();
            DataTable dt = _databaseServices.GetConnectionsForConnectionType(connectionType);
            _connectionsView = dt.DefaultView;
            grdConnections.ItemsSource = _connectionsView;
        }



        private void grdConnections_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (grdConnections.SelectedItem != null)
            {
                btnEdit.IsEnabled = true;
                btnDelete.IsEnabled = true;
                btnTest.IsEnabled = true;
            }
            else
            {
                btnEdit.IsEnabled = false;
                btnDelete.IsEnabled = false;
                btnTest.IsEnabled = false;
            }
        }



        private void MenuFiles_Click(object sender, RoutedEventArgs e)
        {
            OpenFileSettingsWindow();
        }

        protected void OpenFileSettingsWindow()
        {
            if (_fileSettingsWindow != null && _fileSettingsWindow.IsVisible)
            {
                _fileSettingsWindow.Focus();
            }
            else
            {
                _fileSettingsWindow = new FileSettingsWindow();
                _fileSettingsWindow.Owner = this;
                _fileSettingsWindow.Show();
            }
        }

        private void MenuDatabase_Click(object sender, RoutedEventArgs e)
        {
            OpenDatabaseSettingsWindow();
        }

        protected void OpenDatabaseSettingsWindow()
        {
            if (_databaseSettingsWindow != null && _databaseSettingsWindow.IsVisible)
            {
                _databaseSettingsWindow.Focus();
            }
            else
            {
                _databaseSettingsWindow = new DatabaseSettingsWindow();
                _databaseSettingsWindow.Owner = this;
                _databaseSettingsWindow.Show();
            }
        }

        private void MenuWebService_Click(object sender, RoutedEventArgs e)
        {
            OpenWebServiceSettingsWindow();
        }

        private void OpenWebServiceSettingsWindow()
        {
            if (_webServiceSettingsWindow != null && _webServiceSettingsWindow.IsVisible)
            {
                _webServiceSettingsWindow.Focus();
            }
            else
            {
                _webServiceSettingsWindow = new WebServiceSettingsWindow();
                _webServiceSettingsWindow.Owner = this;
                _webServiceSettingsWindow.Show();
            }
        }

        private void MenuApplicationSettings_Click(object sender, RoutedEventArgs e)
        {
            OpenApplicationSettingsWindow();
        }

        private void OpenApplicationSettingsWindow()
        {
            if (_applicationSettingsWindow != null && _applicationSettingsWindow.IsVisible)
            {
                _applicationSettingsWindow.Focus();
            }
            else
            {
                _applicationSettingsWindow = new ApplicationSettingsWindow();
                _applicationSettingsWindow.Owner = this;
                _applicationSettingsWindow.Show();
            }
        }
    }
}
