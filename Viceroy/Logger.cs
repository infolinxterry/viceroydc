﻿using System;
using System.Diagnostics;
using System.IO;

namespace Viceroy
{
    public class Logger
    {
        public string FilePath { get; set; }

        private string _logFilePrefix;

        public Logger(string prefix)
        {
            _logFilePrefix = prefix;

            ResolveLogFilePath();
            SetUpLogger();
            CleanUpLogFiles();
        }
       
        public void WriteMessage(string message)
        {
            try
            {
                message = string.Format("{0:G}: {1}{2}", DateTime.Now, message, Environment.NewLine);

                File.AppendAllText(FilePath, message);
            }
            catch (Exception ex)
            {
                EventLog eventLog = new EventLog();
                eventLog.Source = "Viceroy";
                eventLog.WriteEntry(string.Format("Error writing to log file {0}{1}Exception Message: {2}{3}Stack Trace: {4}", 
                    FilePath, Environment.NewLine, ex.Message, Environment.NewLine, ex.StackTrace));
            }
        }

        private void ResolveLogFilePath()
        {
            if (!Directory.Exists(Properties.Settings.Default.LogFilePath))
            {
                string defaultLogPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log");
                TryToCreateDefaultLogDirectory(defaultLogPath);
            }
        }

        private void TryToCreateDefaultLogDirectory(string defaultLogPath)
        {
            try
            {
                Directory.CreateDirectory(defaultLogPath);

                Properties.Settings.Default.LogFilePath = defaultLogPath;
            }
            catch (UnauthorizedAccessException uae)
            {
                EventLog eventLog = new EventLog();
                eventLog.Source = "Viceroy";
                eventLog.WriteEntry(string.Format("Error creating default log directory{0}Exception Message: {1}{2}Stack Trace: {3}",
                    Environment.NewLine, uae.Message, Environment.NewLine, uae.StackTrace));

                Properties.Settings.Default.LogFilePath = AppDomain.CurrentDomain.BaseDirectory;
            }
            finally
            {
                Properties.Settings.Default.Save();
            }
        }

        private void SetUpLogger()
        {
            string fileName = _logFilePrefix + "_" + DateTime.Now.ToFileTime() + ".log";
            string fullLogFileName = Path.Combine(Properties.Settings.Default.LogFilePath, fileName);

            FilePath = fullLogFileName;
        }

        private void CleanUpLogFiles()
        {
            DirectoryInfo logFileDirectoryInfo = new DirectoryInfo(Properties.Settings.Default.LogFilePath);
            FileInfo[] logFiles = logFileDirectoryInfo.GetFiles(_logFilePrefix + "_" + "*.log");
            TryToDeleteOldLogFiles(logFiles);
        }

        private void TryToDeleteOldLogFiles(FileInfo[] logFiles)
        {
            try
            {
                DeleteOldLogFiles(logFiles);
            }
            catch (Exception ex)
            {
                EventLog eventLog = new EventLog();
                eventLog.Source = "Viceroy";
                eventLog.WriteEntry(string.Format("Error deleting old log files{0}Exception Message: {1}{2}Stack Trace: {3}",
                    Environment.NewLine, ex.Message, Environment.NewLine, ex.StackTrace));
            }
        }

        private void DeleteOldLogFiles(FileInfo[] files)
        {
            foreach (var file in files)
            {
                if (file.CreationTime < DateTime.Now.AddDays(Properties.Settings.Default.DeleteLogFileAfterDays * -1))
                {
                    file.Delete();
                }
            }
        }
        public void LogException(string source, Exception ex)
        {
            string detailedErrorMessage = ex.ToString();

            while (ex.InnerException != null)
            {
                detailedErrorMessage += $" {ex.InnerException.ToString()}";

                ex = ex.InnerException;
            }

            this.WriteMessage(source + " " + detailedErrorMessage);
        }
    }
}
