﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Viceroy
{
    /// <summary>
    /// Interaction logic for ApplicationSettings.xaml
    /// </summary>
    public partial class ApplicationSettingsWindow : Window
    {
        public ApplicationSettingsWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.LoadLists();

            //set values 
            if (Properties.Settings.Default.Frequency != 1)
            {
                this.cboHours.SelectedItem = Properties.Settings.Default.Frequency.ToString() + " hours";
            }
            else
            {
                this.cboHours.SelectedItem = Properties.Settings.Default.Frequency.ToString() + " hour";
            }

            if (Properties.Settings.Default.DirectoryRefreshTime < 12)
            {
                this.cboTime.SelectedItem = Properties.Settings.Default.DirectoryRefreshTime.ToString() + ":00 AM";
            }
            else
            {
                this.cboTime.SelectedItem = Properties.Settings.Default.DirectoryRefreshTime.ToString() + ":00 PM";
            }
        }
        protected void LoadLists()
        {
            this.cboHours.Items.Clear();
            for (int i = 1; i < 25; i++)
            {
                if(i == 1)
                {
                    this.cboHours.Items.Add(i.ToString() + " hour");
                }
                else
                {
                    this.cboHours.Items.Add(i.ToString() + " hours");
                }                
            }

            this.cboHours.SelectedIndex = 0;

            this.cboTime.Items.Add("12:00 AM");

            for (int i = 1; i < 12; i++)
            {
                this.cboTime.Items.Add(i.ToString() + ":00 AM");
            }
            this.cboTime.Items.Add("12:00 PM");
            for (int i = 1; i < 12; i++)
            {
                this.cboTime.Items.Add(i.ToString() + ":00 PM");
            }

            this.cboTime.SelectedIndex = 0;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.Frequency = Convert.ToInt32(this.cboHours.SelectedItem.ToString().Replace(" hours", "").Replace(" hour", ""));
            if (this.cboTime.SelectedItem.ToString().Contains("AM"))
            {
                Properties.Settings.Default.DirectoryRefreshTime = Convert.ToInt32(this.cboTime.SelectedItem.ToString().Replace(":00 AM", ""));
            }
            else
            {
                Properties.Settings.Default.DirectoryRefreshTime = Convert.ToInt32(this.cboTime.SelectedItem.ToString().Replace(":00 PM", "")) + 12;
            }

            Properties.Settings.Default.Save();
            MessageBox.Show("Application settings successfully set!" + Environment.NewLine, "Application Settings", MessageBoxButton.OK, MessageBoxImage.Information);

            MainWindow mw = Owner as MainWindow;           
            mw.WindowLoadSetup();
            this.Cursor = Cursors.Arrow;
            Close();
            Owner.Focus();

            this.Close();
            Owner.Focus();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            Owner.Focus();
        }
    }
}
