﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Viceroy
{
    class InfolinxItem
    {
        /// <summary>
        /// "I_DELETED"
        /// </summary>
        public static string ITEM_COL_I_DELETED     = "I_DELETED";
        /// <summary>
        /// "I_MODIFY_DATE"
        /// </summary>
        public static string ITEM_COL_I_MODIFY_DATE = "I_MODIFY_DATE";
        /// <summary>
        /// "I_ID"
        /// </summary>
        public static string ITEM_COL_I_ID          = "I_ID";
        /// <summary>
        /// "I_IT_ID"
        /// </summary>
        public static string ITEM_COL_I_IT_ID       = "I_IT_ID";

        public InfolinxItem()
        {
            
        }

        public async static Task<bool> EstablishConnectionWithWebService(string UserName, string Password, string Url)
        {
            bool blnRet = false;

            Properties.Settings.Default.UserName = UserName;
            Properties.Settings.Default.Password = StringDecrypter.EncryptString(Password);
            Properties.Settings.Default.InfolinxRestBaseAddress = Url;
            Properties.Settings.Default.Save();          

            IRTab itab = await InfolinxRestProxy.GetTab(1);

            if (itab.Id == 1)
            {
                blnRet = true;
            }

            return blnRet;
        }
        /// <summary>
        /// get all the matters greater than the max i_id we have in the INFOLINX_ITEMS table and insert them into the INFOLINX_ITEMS table
        /// </summary>
        public static async void GetInfolinxMatters()
        {
            DatabaseServices DS = new DatabaseServices(Properties.Settings.Default.ConnectionString);

            int itemID = DS.GetMaxInfolinxItemId();

            try
            {               
                List<IRSimpleItem> allMatters = await GetAllMatters(itemID);

                // Insert these matters into DB
                foreach (var matter in allMatters)
                {
                    DS.InsertIntoInfolinxItemsTable(matter.I, matter.B, matter.D, matter.T, matter.G, Convert.ToInt32(matter.C), matter.R);
                }
            }
            catch (Exception ex)
            {
                Logger logger = new Logger("Viceroy");
                logger.WriteMessage(ex.ToString());
            }
        }

        /// <summary>
        /// gets matters 1000 at a time where the i_id is greater than the one passed in
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns></returns>
        public async static Task<List<IRSimpleItem>> GetAllMatters(int itemID)
        {
            List<IRSimpleItem> allMatters = new List<IRSimpleItem>();
            List<IRSimpleItem> batch = new List<IRSimpleItem>();

            do
            {
                batch = await InfolinxRestProxy.GetMatters(itemID, Properties.Settings.Default.MattersTab, Properties.Settings.Default.MatterCode,
                    Properties.Settings.Default.ClientCode, Properties.Settings.Default.ClientID) as List<IRSimpleItem>;
                allMatters.AddRange(batch);
                if (batch.Count > 0)
                {
                    itemID = batch[batch.Count - 1].I;
                }
            }
            while (batch.Count == 1000);

            return allMatters;
        }

        /// <summary>
        /// we need to get all items that are deleted
        /// </summary>
        public async Task<List<IRSimpleItem>> GetDeletedItemsFromInfolinx()
        {
            List<IRSimpleItem> allExternalItems = new List<IRSimpleItem>();
            List<IRSimpleItem> batch = new List<IRSimpleItem>();

            int itemID = -1;

            Dictionary<string, object> objSearch = new Dictionary<string, object>();

            DatabaseServices ds = new DatabaseServices(Properties.Settings.Default.ConnectionString);

            objSearch.Add(ITEM_COL_I_DELETED, 1);
            objSearch.Add(ITEM_COL_I_MODIFY_DATE, ds.GetMaxModifyDate()); 
            objSearch.Add(ITEM_COL_I_ID, -1);

            do
            {
                batch = await InfolinxRestProxy.GetExternalItems(Properties.Settings.Default.ItemTab, objSearch) as List<IRSimpleItem>;

                objSearch.Remove(ITEM_COL_I_ID);

                allExternalItems.AddRange(batch);
                if (batch.Count > 0)
                {
                    itemID = batch[batch.Count - 1].I;
                    objSearch.Add(ITEM_COL_I_ID, itemID);
                }
            }
            while (batch.Count == 1000);

            return allExternalItems;
        }

        public async void DeleteItemInInfolinx(int ItemID)
        {
            DatabaseServices ds = new DatabaseServices(Properties.Settings.Default.ConnectionString);

            try
            {
                string[] strColumnNames = Properties.Settings.Default.InfolinxColumnNames.Split(",".ToCharArray());
                IRColumn clmiid = new IRColumn();
                clmiid.ColumnName = ITEM_COL_I_ID;
                clmiid.Value = ItemID;              

                IRColumn tabId = new IRColumn();
                tabId.ColumnName = ITEM_COL_I_IT_ID;
                tabId.Value = Properties.Settings.Default.ItemTab; //this is the item we are updating - need this to get the item type data row in web service

                IRItem item = new IRItem();

                item.columns = new List<IRColumn> { };

                item.columns.Add(tabId);
                item.columns.Add(clmiid);
                
                List<IRItem> iList = new List<IRItem>();

                iList.Add(item);

                IRItemResult i = await InfolinxRestProxy.DeleteItem(ItemID);

                //now delete from local table
                if (i.Result == true)
                {
                    ds.DeleteNetworkItemByInfolinxId(ItemID);
                }
               
            }
            catch (Exception ex)
            {
                string s = ex.ToString();
            }
        }
        public async void UpdateItemInInfolinx(int ItemID, int count)
        {
            DatabaseServices ds = new DatabaseServices(Properties.Settings.Default.ConnectionString);

            try
            {
                string[] strColumnNames = Properties.Settings.Default.InfolinxColumnNames.Split(",".ToCharArray());
                IRColumn clmiid = new IRColumn();
                clmiid.ColumnName = ITEM_COL_I_ID;
                clmiid.Value = ItemID;             

                IRColumn clmCount = new IRColumn();
                clmCount.ColumnName = strColumnNames[3]; //count #
                clmCount.Value = count;

                IRColumn tabId = new IRColumn();
                tabId.ColumnName = ITEM_COL_I_IT_ID;
                tabId.Value = Properties.Settings.Default.ItemTab; //this is the item we are updating - need this to get the item type data row in web service

                IRItem item = new IRItem();

                item.columns = new List<IRColumn> { };

                item.columns.Add(tabId);
                item.columns.Add(clmiid);                                
                item.columns.Add(clmCount);               

                List<IRItem> iList = new List<IRItem>();

                iList.Add(item);
                
                IRItemResultList i = await InfolinxRestProxy.CreateItem(iList);

                foreach (IRItemResult iresult in i.ItemResults)
                {
                    ds.RemoveUpdateFlagOnInfolinxItem(ItemID);
                }
            }
            catch(Exception ex)
            {
                string s = ex.ToString();
            }

        }
        public async void CreateItemInInfolinx(string strPath, int count,int LocalID,int ExternalType,string strFolderName,string strFileName,int ClientID)
        {                       
            DatabaseServices ds = new DatabaseServices(Properties.Settings.Default.ConnectionString);

            string strExternalSource = "en-US_" + ExternalType.ToString();

            //Matter is  a list
            int MatterID = ds.GetItemIdForFolderName(strFolderName);

            try
            {

                string[] strColumnNames = Properties.Settings.Default.InfolinxColumnNames.Split(",".ToCharArray());

                IRColumn clmFileName = new IRColumn();
                clmFileName.ColumnName = strColumnNames[0]; //file name
                clmFileName.Value = strFileName;

                IRColumn clmNetWorkPath = new IRColumn();
                clmNetWorkPath.ColumnName = strColumnNames[1]; //network path
                clmNetWorkPath.Value = strPath;

                IRColumn clmMatter = new IRColumn();
                clmMatter.ColumnName = strColumnNames[2]; //matter # - if this is a drop down like it will be for AG we can use the rows in INFOLINX_ITEMS to match text and get the id
                clmMatter.Value = MatterID;

                IRColumn clmCount = new IRColumn();
                clmCount.ColumnName = strColumnNames[3]; //count #
                clmCount.Value = count;

                IRColumn clmExternaleType = new IRColumn();
                clmExternaleType.ColumnName = strColumnNames[4]; //exteranl type # Access, File Share - ...
                clmExternaleType.Value = strExternalSource;


                IRColumn clmClientID = new IRColumn();
                clmClientID.ColumnName = Properties.Settings.Default.ClientID;
                clmClientID.Value = ClientID;



                IRColumn tabId = new IRColumn();
                tabId.ColumnName = ITEM_COL_I_IT_ID;
                tabId.Value = Properties.Settings.Default.ItemTab; //this is the item we are creating

                IRItem item = new IRItem();

                item.columns = new List<IRColumn> { };

                item.columns.Add(tabId);

                item.columns.Add(clmFileName);
                item.columns.Add(clmNetWorkPath);
                item.columns.Add(clmMatter);
                item.columns.Add(clmCount);
                item.columns.Add(clmExternaleType);
                item.columns.Add(clmClientID);

                List<IRItem> iList = new List<IRItem>();

                iList.Add(item);

                IRItemResultList i = await InfolinxRestProxy.CreateItem(iList);

                foreach (IRItemResult iresult in i.ItemResults)
                {
                    ds.UpdateItemsNetworkTable(LocalID, iresult.ItemId);
                }                
            }
            catch(Exception ex)
            {
                string s = ex.ToString();
            }
        }
    }
}
