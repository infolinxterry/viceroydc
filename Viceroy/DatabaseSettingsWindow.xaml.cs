﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Input;

namespace Viceroy
{
    /// <summary>
    /// Interaction logic for DatabaseSettingsWindow.xaml
    /// </summary>
    public partial class DatabaseSettingsWindow : Window
    {
        public DatabaseSettingsWindow()
        {
            InitializeComponent();
        }

        private void WindowsAuthenticationCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (Convert.ToBoolean(WindowsAuthenticationCheckBox.IsChecked))
            {
                UsernameTextBox.IsEnabled = false;
                PasswordBox.IsEnabled = false;
            }
            else
            {
                UsernameTextBox.IsEnabled = true;
                PasswordBox.IsEnabled = true;
            }
        }

        protected void LoadControlsWithConnectionInfo()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(Properties.Settings.Default.ConnectionString);

            ServerInstanceTextBox.Text = builder["DATA SOURCE"].ToString();
            DatabaseTextBox.Text = builder["INITIAL CATALOG"].ToString();

            if (Properties.Settings.Default.ConnectionString.Contains("UID="))
            {
                UsernameTextBox.Text = builder["UID"].ToString();
                PasswordBox.Password = builder["PWD"].ToString();
            }
            else
            {
                WindowsAuthenticationCheckBox.IsChecked = true;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadControlsWithConnectionInfo();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
            Owner.Focus();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {

            this.Cursor = Cursors.Wait;

            string strConnectionsString = "DATA SOURCE=tsbutler;INITIAL CATALOG=INFOLINX_EXTERNAL_DATA;";// UID=sa;PWD=infolinx;

            strConnectionsString = strConnectionsString.Replace("tsbutler", ServerInstanceTextBox.Text);
            strConnectionsString = strConnectionsString.Replace("INFOLINX_EXTERNAL_DATA", DatabaseTextBox.Text);

            if (Convert.ToBoolean(WindowsAuthenticationCheckBox.IsChecked))
            {
                strConnectionsString += "Integrated Security=SSPI;";
            }
            else
            {
                strConnectionsString += "UID=" + UsernameTextBox.Text + ";PWD=" + PasswordBox.Password + ";";
            }

            // now test it
            DatabaseServices ds = new DatabaseServices(strConnectionsString);

            try
            {
                DataTable dt = ds.GetConnections();
                MessageBox.Show("Database credentials successfully set!" + Environment.NewLine, "Configure Database Settings", MessageBoxButton.OK, MessageBoxImage.Information);
                Properties.Settings.Default.ConnectionString = strConnectionsString;
                Properties.Settings.Default.Save();
                MainWindow mw = Owner as MainWindow;
                mw.DBConnectionValidated = true;
                mw.WindowLoadSetup();
                this.Cursor = Cursors.Arrow;
                Close();
                Owner.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show("There was an error connecting to the database!" + Environment.NewLine + ex.Message + Environment.NewLine + "Please re-enter database credentials.", "Configure Database Settings", MessageBoxButton.OK, MessageBoxImage.Error);
                this.Cursor = Cursors.Arrow;
            }
        }
    }
}
