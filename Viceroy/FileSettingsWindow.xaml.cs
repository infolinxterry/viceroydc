﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Forms;
namespace Viceroy
{
    /// <summary>
    /// Interaction logic for FilesSettings.xaml
    /// </summary>
    public partial class FileSettingsWindow : Window
    {
        public bool InvalidLogPath { get; set; }
        public FileSettingsWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if(Directory.Exists(Properties.Settings.Default.LogFilePath))
            {
                this.LogFilePathTextBox.Text = Properties.Settings.Default.LogFilePath;
            }
            else
            {
                this.LogFilePathTextBox.Text = System.AppDomain.CurrentDomain.BaseDirectory;
            }

            if (Directory.Exists(Properties.Settings.Default.ArchiveLocation)) //this can be blank they may not be archiving files
            {
                this.ArchivePathTextBox.Text = Properties.Settings.Default.ArchiveLocation;
            }

            this.chkVerboseLogging.IsChecked = Properties.Settings.Default.VerboseLogging;
            
        }

        private void CloseWindow()
        {
            MainWindow mw = Owner as MainWindow;
            if (mw.ShowFilesSettingsWindow)
            {
                mw.ShowFilesSettingsWindow = false;               
                mw.Focus();
                this.Hide();
                mw.WindowLoadSetup();
            }
            this.Close();
            Owner.Focus();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (Directory.Exists(this.LogFilePathTextBox.Text))
            {
                Properties.Settings.Default.LogFilePath = this.LogFilePathTextBox.Text;
                Properties.Settings.Default.VerboseLogging = Convert.ToBoolean(this.chkVerboseLogging.IsChecked);

                Properties.Settings.Default.Save();
                System.Windows.MessageBox.Show("Files settings successfully set!" + Environment.NewLine, "Configure File Settings", MessageBoxButton.OK, MessageBoxImage.Information);                

                if (this.ArchivePathTextBox.Text != String.Empty)
                {
                    if (Directory.Exists(this.ArchivePathTextBox.Text))
                    {
                        Properties.Settings.Default.ArchiveLocation = this.ArchivePathTextBox.Text;
                        Properties.Settings.Default.Save();
                        this.CloseWindow();
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("The path for the archiving files is invalid" + Environment.NewLine + "Please enter a valid path.", "Configure File Settings", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    this.CloseWindow();
                }               
            }
            else
            {
                System.Windows.MessageBox.Show("The path for the log file is invalid" + Environment.NewLine + "Please enter a valid path.", "Configure File Settings", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            if(this.ArchivePathTextBox.Text != String.Empty)
            {
                if (Directory.Exists(this.ArchivePathTextBox.Text))
                {
                    Properties.Settings.Default.ArchiveLocation = this.ArchivePathTextBox.Text;
                    Properties.Settings.Default.Save();
                }
                else
                {
                    System.Windows.MessageBox.Show("The path for the archiving files is invalid" + Environment.NewLine + "Please enter a valid path.", "Configure File Settings", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnArchive_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            DialogResult result = dialog.ShowDialog();
            if (result.ToString() == "OK")
            {
                this.ArchivePathTextBox.Text = dialog.SelectedPath;
            }
        }

        private void btnLog_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            DialogResult result = dialog.ShowDialog();
            if (result.ToString() == "OK")
            {
                this.LogFilePathTextBox.Text = dialog.SelectedPath;
            }
        }
    }
}
