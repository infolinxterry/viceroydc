﻿using System.Collections.Generic;
using System.IO;

namespace Viceroy
{
    public class FileShareAgent
    {
        public static void GetFilesForConnections()
        {
            // Not sure how the implementation for this got lost...
        }

        public static List<string> FindFilesInDirectoriesMatchingMatterName(string baseDirectory, string matterName)
        {
            List<string> files = new List<string>();

            IEnumerable<string> directories = GetDirectoriesMatchingMatterName(baseDirectory, matterName);
            foreach (var directory in directories)
            {
                files.AddRange(Directory.GetFiles(directory));
            }

            return files;
        }

        public static Dictionary<string, int> GetFileCountsFromDirectoriesMatchingMatterName(string baseDirectory, string matterName)
        {
            Dictionary<string, int> counts = new Dictionary<string, int>();

            IEnumerable<string> directories = GetDirectoriesMatchingMatterName(baseDirectory, matterName);
            foreach (var directory in directories)
            {
                counts.Add(directory, Directory.GetFiles(directory).Length);
            }

            return counts;
        }

        public static IEnumerable<string> GetDirectoriesMatchingMatterName(string baseDirectory, string matterName)
        {
            List<string> directories = new List<string>();

            if (baseDirectory.Contains(matterName))
            {
                directories.Add(baseDirectory);
            }

            string[] subdirectories = Directory.GetDirectories(baseDirectory, matterName, SearchOption.AllDirectories);
            directories.AddRange(subdirectories);

            return directories;
        }
    }
}
