﻿namespace Viceroy
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Enum for transfer results (success and various types of failures)
    /// </summary>
    public enum TransferResult
    {
        /// <summary>
        /// The transfer succeeded.
        /// </summary>
        Success,

        /// <summary>
        /// The transfer failed because the location could not contain the item being transferred.
        /// </summary>
        FailedLocationCannotContainItem,

        /// <summary>
        /// The transfer failed because the location barcode was not recognized. 
        /// </summary>
        FailedLocationBarcodeNotFound,

        /// <summary>
        /// The transfer failed because the barcode of the item being transferred was not recognized.
        /// </summary>
        FailedItemBarcodeNotFound,

        /// <summary>
        /// The transfer failed because the item being transferred was immovable.
        /// </summary>
        FailedItemNotMoveable,

        /// <summary>
        /// The transfer failed for some other unknown reason.
        /// </summary>
        FailedUnknownReason
    }

    /// <summary>
    /// 
    /// </summary>
    public partial class InfolinxRestProxy
    {
        public static async Task CheckConnectivity()
        {
            var handler = new HttpClientHandler
            {
                Proxy = WebRequest.GetSystemWebProxy(),
                UseProxy = true
            };

            using (var client = new TestClient(handler))
            {
                SetupHttpClient(client.HttpClient);
                var response = await client.GetAsync().ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
            }
        }

        public static async Task<IRTab> GetTab(int it_id)
        {
            var handler = new HttpClientHandler
            {
                Proxy = WebRequest.GetSystemWebProxy(),
                UseProxy = true
            };

            using (var client = new TabClient(handler))
            {
                SetupHttpClient(client.HttpClient);
                var response = await client.GetAsync(it_id);
                response.EnsureSuccessStatusCode();
                IRTab content = await response.Content.ReadAsAsync<IRTab>();
                return content;
            }
        }

        public static async Task<IEnumerable<IRTab>> GetAllTabs()
        {
            var handler = new HttpClientHandler
            {
                Proxy = WebRequest.GetSystemWebProxy(),
                UseProxy = true
            };

            using (var client = new TabClient(handler))
            {
                SetupHttpClient(client.HttpClient);
                var response = await client.GetAsync().ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                var content = await response.Content.ReadAsAsync<IEnumerable<IRTab>>().ConfigureAwait(false);
                return content;
            }
        }

        public async static Task<IRItem> GetItemByBarcode(string barcode)
        {
            var handler = new HttpClientHandler
            {
                Proxy = WebRequest.GetSystemWebProxy(),
                UseProxy = true
            };

            using (var client = new ItemClient(handler))
            {
                SetupHttpClient(client.HttpClient);

                HttpResponseMessage response = await client.PostAsync(new IRItemSearch
                {
                    Barcode = barcode
                }).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();

                IEnumerable<IRItem> items = await response.Content.ReadAsAsync<IEnumerable<IRItem>>().ConfigureAwait(false);

                IRItem item = null;

                if (items != null)
                {
                    List<IRItem> listitems = items as List<IRItem>;

                    if (listitems.Count > 0)
                    {
                        item = listitems[0];
                    }
                }

                return item;
            }
        }

        public async static Task<bool> GetMaxItemsOut(int user_i_id, int it_id)
        {
            var handler = new HttpClientHandler
            {
                Proxy = WebRequest.GetSystemWebProxy(),
                UseProxy = true
            };

            using (var client = new MaxItemsOutClient(handler))
            {
                SetupHttpClient(client.HttpClient);
                var response = await client.GetAsync(user_i_id, it_id).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsAsync<bool>();
            }
        }

        public static async Task<IEnumerable<IRPicklistItem>> GetPicklistItems(int id)
        {
            var handler = new HttpClientHandler
            {
                Proxy = WebRequest.GetSystemWebProxy(),
                UseProxy = true
            };

            using (var client = new PicklistClient(handler))
            {
                SetupHttpClient(client.HttpClient);
                var response = await client.GetAsync(id);
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsAsync<IEnumerable<IRPicklistItem>>();
            }
        }

        public static async Task<IEnumerable<IRItem>> GetMissingItems(int id)
        {
            var handler = new HttpClientHandler
            {
                Proxy = WebRequest.GetSystemWebProxy(),
                UseProxy = true
            };

            using (var client = new ItemClient(handler))
            {
                SetupHttpClient(client.HttpClient);
                var response = await client.PostAsync(new IRItemSearch()
                {
                    SpecialSearch = "missing",
                    TabId = id
                });
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsAsync<IEnumerable<IRItem>>();
            }
        }

        public static async Task<IEnumerable<IRItem>> GetItemByText(int id, Dictionary<string,object> objSearchTerms)
        {
            var handler = new HttpClientHandler
            {
                Proxy = WebRequest.GetSystemWebProxy(),
                UseProxy = true
            };

            using (var client = new ItemClient(handler))
            {
                SetupHttpClient(client.HttpClient);
                var response = await client.PostAsync(new IRItemSearch()
                {
                    SpecialSearch = "like",
                    SearchTerms = objSearchTerms,
                    TabId = id                   
                });
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsAsync<IEnumerable<IRItem>>();
            }
        }

        public static async Task<IEnumerable<IRTab>> GetLocationTabs()
        {
            var handler = new HttpClientHandler
            {
                Proxy = WebRequest.GetSystemWebProxy(),
                UseProxy = true
            };

            using (var client = new TabClient(handler))
            {
                SetupHttpClient(client.HttpClient);
                var response = await client.GetAsync("topshelfuser");
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsAsync<IEnumerable<IRTab>>();
            }
        }

        public static async Task<IEnumerable<IRTab>> GetPicklistTabs()
        {
            var handler = new HttpClientHandler
            {
                Proxy = WebRequest.GetSystemWebProxy(),
                UseProxy = true
            };

            using (var client = new TabClient(handler))
            {
                SetupHttpClient(client.HttpClient);
                var response = await client.GetAsync("picklist");
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsAsync<IEnumerable<IRTab>>();
            }
        }

        public async static Task<IRItem> GetItemById(Int32 id)
        {
            var handler = new HttpClientHandler
            {
                Proxy = WebRequest.GetSystemWebProxy(),
                UseProxy = true
            };

            using (var client = new ItemClient(handler))
            {
                SetupHttpClient(client.HttpClient);
                var response = await client.GetAsync(id);
                response.EnsureSuccessStatusCode();
                IRItem item = await response.Content.ReadAsAsync<IRItem>();
                return item;
            }
        }

        public async static Task<IEnumerable<IRItem>> GetItemsByTab(int it_id)
        {
            var handler = new HttpClientHandler
            {
                Proxy = WebRequest.GetSystemWebProxy(),
                UseProxy = true
            };

            using (var client = new ItemClient(handler))
            {
                SetupHttpClient(client.HttpClient);
                var response = await client.PostAsync(new IRItemSearch
                {
                    TabId = it_id
                });
                response.EnsureSuccessStatusCode();
                var content = await response.Content.ReadAsAsync<IEnumerable<IRItem>>();
                return content;
            }
        }

        public async static Task<IRItemResultList> TransferItems(IRTransfer t)
        {
            var handler = new HttpClientHandler
            {
                Proxy = WebRequest.GetSystemWebProxy(),
                UseProxy = true
            };

            using (var client = new TransferClient(handler))
            {
                SetupHttpClient(client.HttpClient);
                HttpResponseMessage response = await client.PostAsync(t).ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                IRItemResultList results = await response.Content.ReadAsAsync<IRItemResultList>().ConfigureAwait(false);

                return results;
            }
        }

        protected static void SetupHttpClient(HttpClient client)
        {
            string strUserName = Properties.Settings.Default.UserName;
            string strPassword = StringDecrypter.DecryptString(Properties.Settings.Default.Password);

            client.BaseAddress = new Uri(Properties.Settings.Default.InfolinxRestBaseAddress);

            client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");

            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes(strUserName + ":" + strPassword)));
        }

        public async static Task<IRIO> PostAsyncInChuncks(IRIO objIO)
        {
            var handler = new HttpClientHandler
            {
                Proxy = WebRequest.GetSystemWebProxy(),
                UseProxy = true
            };

            using (var client = new IRIOClient(handler))
            {
                SetupHttpClient(client.HttpClient);
                var response = await client.PostAsyncInChuncks(objIO);
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsAsync<IRIO>();
            }
        }


        public async static Task<IRItemResult> DeleteItem(int id)
        {
            var handler = new HttpClientHandler
            {
                Proxy = WebRequest.GetSystemWebProxy(),
                UseProxy = true
            };

            using (var client = new ItemClient(handler))
            {
                SetupHttpClient(client.HttpClient);
                var response = await client.DeleteAsync(id);
                response.EnsureSuccessStatusCode();
                var content = await response.Content.ReadAsAsync<IRItemResult>();
                return content;
            }
        }



        public async static Task<IRItemResultList> CreateItem(List<IRItem> lstItem)
        {
            var handler = new HttpClientHandler
            {
                Proxy = WebRequest.GetSystemWebProxy(),
                UseProxy = true
            };

            using (var client = new ItemClient(handler))
            {
                SetupHttpClient(client.HttpClient);
                var response = await client.PostAsync(lstItem);
                response.EnsureSuccessStatusCode();
                var content = await response.Content.ReadAsAsync<IRItemResultList>();
                return content;
            }
        }

        public async static Task<IRItemResultList> CreateEdocItem(List<IRItem> lstItem)
        {
            var handler = new HttpClientHandler
            {
                Proxy = WebRequest.GetSystemWebProxy(),
                UseProxy = true
            };

            using (var client = new ItemClient(handler))
            {
                SetupHttpClient(client.HttpClient);
                var response = await client.PostAsync(lstItem);
                response.EnsureSuccessStatusCode();
                var content = await response.Content.ReadAsAsync<IRItemResultList>();
                return content;
            }
        }

        public async static Task<IEnumerable<IRDictionary>> GetQuickSearchFields(IRDictionarySearch Irds)
        {
            var handler = new HttpClientHandler
            {
                Proxy = WebRequest.GetSystemWebProxy(),
                UseProxy = true
            };

            using (var client = new DictionaryClient(handler))
            {
                SetupHttpClient(client.HttpClient);
                var response = await client.PostAsync(Irds);
                response.EnsureSuccessStatusCode();
                var content = await response.Content.ReadAsAsync<IEnumerable<IRDictionary>>();
                return content;
            }
        }

        public async static Task<IEnumerable<IRSimpleItem>> GetMatters(int i_id, int it_id, string GColumnName, string RColumnName, string CColumnName)
        {
            var handler = new HttpClientHandler
            {
                Proxy = WebRequest.GetSystemWebProxy(),
                UseProxy = true
            };

            using (var client = new SimpleClient(handler))
            {
                SetupHttpClient(client.HttpClient);
                var response = await client.GetAsync(i_id, it_id, GColumnName, RColumnName, CColumnName);
               
                response.EnsureSuccessStatusCode();
                var content = await response.Content.ReadAsAsync<IEnumerable<IRSimpleItem>>();
                return content;
            }
        }

        /// <summary>
        /// get all of the items in Infolinx that were created from external sources
        /// </summary>
        /// <param name="i_id"></param>
        /// <param name="it_id"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public async static Task<IEnumerable<IRSimpleItem>> GetExternalItems(int id, Dictionary<string, object> objSearchTerms)
        {
            var handler = new HttpClientHandler
            {
                Proxy = WebRequest.GetSystemWebProxy(),
                UseProxy = true
            };

            using (var client = new SimpleClient(handler))
            {
                SetupHttpClient(client.HttpClient);
                var response = await client.PostAsync(new IRItemSearch()
                {
                    SpecialSearch = "none",
                    SearchTerms = objSearchTerms,
                    TabId = id
                });
                response.EnsureSuccessStatusCode();
                var content = await response.Content.ReadAsAsync<IEnumerable<IRSimpleItem>>();
                return content;
            }
        }


    }
}
