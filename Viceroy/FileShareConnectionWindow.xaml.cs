﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Forms;

namespace Viceroy
{
    /// <summary>
    /// Interaction logic for ConfigureFileShareConnectionWindow.xaml
    /// </summary>
    public partial class FileShareConnectionWindow : Window
    {
        public int ConnectionID { get; set; }
        public string ConnectionName { get; set; }
        public string NetworkPath { get; set; }
        public string Filter { get; set; }
        public bool IndexContent { get; set; }
        public bool IncludeSubdirectories { get; set; }   
        public bool UpdateConnection { get; set; }
        public string FilterInclude { get; set; }
        public string SearchPattern { get; set; }
        public bool Archive { get; set; }

        public FileShareConnectionWindow()
        {
            InitializeComponent();
    
            ConnectionName = string.Empty;
            NetworkPath = string.Empty;
            Filter = "*.*";
            IndexContent = false;
            IncludeSubdirectories = false;
            UpdateConnection = false;
            SearchPattern = "%";
            Archive = false;
        }

        private void TestConnectionButton_Click(object sender, RoutedEventArgs e)
        {           
            if (this.TestConnection())
            {
                System.Windows.MessageBox.Show("Connection successful!", "Configure File Share Connection", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {           
            if (this.TestConnection())
            {
                if(this.SaveRepository())
                {
                    MainWindow mw = Owner as MainWindow;
                    mw.BindGridForSelectedConnectionType();
                    this.Close();
                    Owner.Focus();
                }
            }
        }
        private bool TestConnection()
        {
            bool blnRet = true;

            if (this.FilterTextBox.Text.Length > 0)
            {
                try
                {
                    string[] aryFilters = this.FilterTextBox.Text.Split(',');
                    foreach (string filter in aryFilters)
                    {
                        //they should start with an *
                        if (!filter.StartsWith("*"))
                        {
                            System.Windows.MessageBox.Show("There is an error in the filter that was entered!" + Environment.NewLine + "Please enter a valid filter." + Environment.NewLine +
                               "Filters should start with an asterick followed by a period, followed by the desired extension." + Environment.NewLine +
                               "Additional extensions should be entered preceded by a comma!", "Configure File Share Connection", MessageBoxButton.OK, MessageBoxImage.Error);
                            return false;
                        }
                        else
                        {
                            try
                            {
                                if (filter.Substring(1, 1) != ".")
                                {
                                    System.Windows.MessageBox.Show("There is an error in the filter that was entered!" + Environment.NewLine + "Please enter a valid filter." + Environment.NewLine +
                                    "Filters should start with an asterick followed by a period, followed by the desired extension." + Environment.NewLine +
                                    "Additional extensions should be entered preceded by a comma!", "Configure File Share Connection", MessageBoxButton.OK, MessageBoxImage.Error);
                                    return false;
                                }
                            }
                            catch
                            {
                                System.Windows.MessageBox.Show("There is an error in the filter that was entered!" + Environment.NewLine + "Please enter a valid filter." + Environment.NewLine +
                                   "Filters should start with an asterick followed by a period, followed by the desired extension." + Environment.NewLine +
                                   "Additional extensions should be entered preceded by a comma!", "Configure File Share Connection", MessageBoxButton.OK, MessageBoxImage.Error);
                                return false;
                            }
                        }
                    }
                }
                catch
                {
                    System.Windows.MessageBox.Show("There is an error in the filter that was entered!" + Environment.NewLine + "Please enter a valid filter." + Environment.NewLine +
                                "Filters should start with an asterick followed by a period, followed by the desired extension." + Environment.NewLine +
                                "Additional extensions should be entered preceded by a comma!", "Configure File Share Connection", MessageBoxButton.OK, MessageBoxImage.Error);
                    return false;
                }
            }

            //verify 
            if (Directory.Exists(this.DriveTextBox.Text))
            {
                //System.Windows.MessageBox.Show("Connection successful!", "Configure File Share Connection", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                System.Windows.MessageBox.Show("Connection not successful!" + Environment.NewLine + "Please enter a valid path.", "Configure File Share Connection", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            return blnRet;
        }

        private bool SaveRepository()
        {
            bool Saved = false;

            if (this.TestConnection())
            {
                this.ConnectionName = this.txtName.Text;
                this.NetworkPath = this.DriveTextBox.Text;
                this.Filter = this.FilterTextBox.Text;              
                this.SearchPattern = this.txtSearchPattern.Text;
                this.IndexContent = Convert.ToBoolean(this.IndexContentCheckBox.IsChecked);
                this.IncludeSubdirectories = Convert.ToBoolean(this.IncludeSubdirectoriesCheckBox.IsChecked);
                this.Archive = this.ArchiveDeleteComboBox.Text == "Archive" ? true : false;
                this.FilterInclude = this.cboIncludeExclude.Text;

                //for now we can just save this in the local database
                DatabaseServices ds = new DatabaseServices(Properties.Settings.Default.ConnectionString);
                if (this.txtName.Text.Length > 0)
                {
                    if (this.DriveTextBox.Text.Length > 0)
                    {
                        if (!this.UpdateConnection && !ds.IsConnectionUnique(this.ConnectionName, this.NetworkPath, this.Filter, this.IndexContent))
                        {
                            System.Windows.MessageBox.Show("There was an error adding this Repository!" + Environment.NewLine + "The combination of Connection Type, Network Path, Filter and Index Content must be unique." + Environment.NewLine +
                           "Please use the Configure button to configure Repository.", "Add Repository", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                            return Saved;
                        }

                        

                        if(this.Archive)
                        {
                            if(!Directory.Exists(Properties.Settings.Default.ArchiveLocation))
                            {
                                if (this.UpdateConnection)
                                {
                                    System.Windows.MessageBox.Show("There was an error updating this Repository!" + Environment.NewLine + "The Archive path is not valid!" + Environment.NewLine +
                                                              "Please use the Files menu pick under the Settings menu to set a valid Archive Path.", "Add Repository", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                                    return Saved;
                                }
                                else
                                {
                                    System.Windows.MessageBox.Show("There was an error adding this Repository!" + Environment.NewLine + "The Archive path is not valid!" + Environment.NewLine +
                          "Please use the Files menu pick under the Settings menu to set a valid Archive Path.", "Add Repository", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                                    return Saved;
                                }

                            }
                        }
                        
                        if (this.UpdateConnection)
                        {                           
                            if (ds.UpdateNetworkRepositoryData(this.ConnectionID, this.ConnectionName, MainWindow.FILE_SHARE_REPOSITORY_TYPE_NAME, this.NetworkPath, this.Filter, this.IndexContent, this.IncludeSubdirectories, this.Archive, this.FilterInclude, this.SearchPattern) == 1)
                            {
                                Saved = true;
                                System.Windows.MessageBox.Show("Repository was successfully updated!" + Environment.NewLine, "Update Repository", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);
                            }
                        }
                        else
                        {
                           
                            if (ds.InsertNetworkRepositoryData(this.ConnectionName, MainWindow.FILE_SHARE_REPOSITORY_TYPE_NAME, this.NetworkPath, this.Filter,this.IndexContent
                                ,this.IncludeSubdirectories, this.Archive, this.FilterInclude, this.SearchPattern) == 1)
                            {
                                Saved = true;
                                System.Windows.MessageBox.Show("Repository was successfully created!" + Environment.NewLine, "Add Repository", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);
                            }
                        }
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("There was an error adding this Repository!" + Environment.NewLine + "Drive path not specified!" + Environment.NewLine +
                            "Please use the Configure button to configure this Repository.", "Add Repository", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                        return Saved;
                    }
                }
                else
                {
                    System.Windows.MessageBox.Show("There was an error adding this Repository!" + Environment.NewLine +
                        "Please enter a valid Repository Name.", "Add Repository", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                    return Saved;
                }
            }

            return Saved;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            DialogResult result = dialog.ShowDialog();
            if(result.ToString()== "OK")
            {
                this.DriveTextBox.Text = dialog.SelectedPath;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.ConnectionName.Length > 0)
            {
                this.txtName.Text = this.ConnectionName;
            }
            if (this.NetworkPath.Length > 0)
            {
                this.DriveTextBox.Text = this.NetworkPath;
            }
            this.FilterTextBox.Text = this.Filter;
            this.IndexContentCheckBox.IsChecked = this.IndexContent;
            this.IncludeSubdirectoriesCheckBox.IsChecked = this.IncludeSubdirectories;
            if (this.Archive)
            {
                this.ArchiveDeleteComboBox.SelectedIndex = 0;
            }
            this.txtSearchPattern.Text = this.SearchPattern;
            if (this.FilterInclude == "Include")
            {
                this.cboIncludeExclude.SelectedIndex = 0;
            }
            else
            {
                this.cboIncludeExclude.SelectedIndex = 1;
            }
        }

        private void ArchiveDeleteComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            LoadArchiveDeleteComboBox();

            // Default to Delete
            if (this.Archive)
            {
                this.ArchiveDeleteComboBox.SelectedIndex = 0;
            }
            else
            {
                ArchiveDeleteComboBox.SelectedIndex = 1;
            }
        }

        private void LoadArchiveDeleteComboBox()
        {
            this.ArchiveDeleteComboBox.Items.Add("Archive");
            this.ArchiveDeleteComboBox.Items.Add("Delete");
        }

        private void DriveTextBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(DriveTextBox.Text))
            {
                TestConnectionButton.IsEnabled = true;

                if (!string.IsNullOrWhiteSpace(txtName.Text))
                {
                    OkButton.IsEnabled = true;
                }
            }
            else
            {
                TestConnectionButton.IsEnabled = false;
                OkButton.IsEnabled = false;
            }
        }

        private void txtName_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtName.Text) && !string.IsNullOrWhiteSpace(DriveTextBox.Text))
            {
                OkButton.IsEnabled = true;
            }
            else
            {
                OkButton.IsEnabled = false;
            }
        }

        private void cboIncludeExclude_Loaded(object sender, RoutedEventArgs e)
        {
            this.cboIncludeExclude.Items.Add("Include");
            this.cboIncludeExclude.Items.Add("Exclude");
            if (this.FilterInclude == null || this.FilterInclude == String.Empty)
            {
                this.cboIncludeExclude.SelectedIndex = 0;
            }
        }
    }
}
