﻿using System;
using System.Data;
using System.Windows;
using System.Windows.Input;

namespace Viceroy
{
    /// <summary>
    /// Interaction logic for InfolinxWebServiceConnection.xaml
    /// </summary>
    public partial class WebServiceSettingsWindow : Window
    {
        public WebServiceSettingsWindow()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;

            this.UsernameTextBox.Text = Properties.Settings.Default.UserName;
            this.PasswordBox.Password = StringDecrypter.DecryptString(Properties.Settings.Default.Password);
            this.UrlTextBox.Text = Properties.Settings.Default.InfolinxRestBaseAddress;
        }

        private async void TestConnection()
        {
            bool blnRet = false;

            try
            {
                blnRet = await InfolinxItem.EstablishConnectionWithWebService(this.UsernameTextBox.Text, this.PasswordBox.Password, this.UrlTextBox.Text);
            }
            catch
            {
                MessageBox.Show("An error occurred connecting to the Infolinx web service!" + Environment.NewLine + "Please enter valid credentials."
                              , "Configure Web Service Connection", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            MessageBox.Show("Connection successful!", "Configure Web Service Connection", MessageBoxButton.OK, MessageBoxImage.Information);

            DatabaseServices DS = new DatabaseServices(Properties.Settings.Default.ConnectionString);

            DataTable dt = DS.GetVRSettings();
            bool blnUserNameExists = false;
            bool blnPasswordExists = false;
            bool blnUrlExists = false;
            foreach(DataRow row in dt.Rows)
            {
                if (row["SETTING_NAME"] != DBNull.Value && row["SETTING_NAME"].ToString() == "UserName")
                {
                    blnUserNameExists = true;
                }
                if (row["SETTING_NAME"] != DBNull.Value && row["SETTING_NAME"].ToString() == "Password")
                {
                    blnPasswordExists = true;
                }
                if (row["SETTING_NAME"] != DBNull.Value && row["SETTING_NAME"].ToString() == "InfolinxRestBaseAddress")
                {
                    blnUrlExists = true;
                }

            }

            if (!blnUserNameExists)
            {
                DS.InsertVRSettings("UserName", "String", "User", Properties.Settings.Default.UserName);
            }
            else
            {
                DS.UpdateVRSettings("UserName", "String", "User", Properties.Settings.Default.UserName);
            }
            if (!blnPasswordExists)
            {
                DS.InsertVRSettings("Password", "String", "User", Properties.Settings.Default.Password);
            }
            else
            {
                DS.UpdateVRSettings("Password", "String", "User", Properties.Settings.Default.Password);
            }
            if (!blnUrlExists)
            {
                DS.InsertVRSettings("InfolinxRestBaseAddress", "String", "User", Properties.Settings.Default.InfolinxRestBaseAddress);
            }
            else
            {
                DS.UpdateVRSettings("InfolinxRestBaseAddress", "String", "User", Properties.Settings.Default.InfolinxRestBaseAddress);
            }

            MainWindow mw = Owner as MainWindow;
            if (mw != null)
            {
                mw.Focus();
            }

            this.Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Cursor = Cursors.Wait;

            TestConnection();

            Cursor = Cursors.Arrow;

        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void UrlTextBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            Uri uriResult;

            if (!string.IsNullOrWhiteSpace(UrlTextBox.Text) && Uri.TryCreate(UrlTextBox.Text, UriKind.Absolute, out uriResult))
            {
                SaveButton.IsEnabled = true;
            }
            else
            {
                SaveButton.IsEnabled = false;
            }
        }
    }
}
