using System;
using System.Data;
using System.Data.SqlClient;

namespace ViceroyService
{
    /// <summary>
    /// Use this class to insert into local database
    /// </summary>
    public class DatabaseServices
    {
        private const string INSERT_ITEMS_INTO_NETWORK_ITEMS_PROC = "INSERT_ITEMS_INTO_NETWORK_ITEMS";
        private const string UPDATE_ITEM_IN_LOCAL_DB_PROC = "UPDATE_ITEMS_LDB";
        private const string INSERT_NETWORK_CONNECTION = "INSERT_NETWORK_CONNECTION";
        private const string GET_CONNECTIONS = "GET_CONNECTIONS";
        private const string INSERT_INFOLINX_ITEMS_PROC = "INSERT_INFOLINX_ITEMS";
        private const string GET_INFOLINX_ITEMS_PROC = "GET_INFOLINX_ITEMS";
        private const string GET_ID_FOR_FOLDER_NAME_PROC = "GET_ID_FOR_FOLDER_NAME";
        private const string GET_NETWORK_ITEMS_PROC = "GET_NETWORK_ITEMS";
        private const string UPDATE_MATTER_ID_IN_NETWORK_TABLE_PROC = "UPDATE_MATTER_ID_IN_NWI";
        private const string GET_MAX_ITEM_ID_PROC = "GET_MAX_ITEM_ID";
        private const string GET_NETWORK_ITEM_FOR_PATH_PROC = "GET_NETWORK_ITEM_FOR_PATH";
        private const string GET_NETWORK_ITEM_FOR_PATH_AND_COUNT_PROC = "GET_NETWORK_ITEM_FOR_PATH_AND_COUNT";
        private const string UPDATE_ITEM_IN_NETWORK_ITEMS_PROC = "UPDATE_ITEM_IN_NETWORK_ITEMS";
        private const string UPDATE_ITEMS_REMOVE_UPD_FLAG_PROC = "UPDATE_ITEMS_REMOVE_UPD_FLAG";
        private const string DELETE_NETWORK_ITEM_BY_INFOLINX_ID_PROC = "DELETE_NETWORK_ITEM_BY_INFOLINX_ID";
        private const string GET_NETWORK_ITEMS_TO_INSERT_OR_UPDATE_PROC = "GET_NETWORK_ITEMS_TO_INSERT_OR_UPDATE";
        private const string GET_INDEXED_NETWORK_ITEMS_PROC = "GET_INDEXED_NETWORK_ITEMS";
        private const string GET_MAX_MODIFY_DATE_PROC = "GET_MAX_MODIFY_DATE";
        private const string INSERT_DELETED_INFOLINX_ITEMS_PROC = "INSERT_DELETED_INFOLINX_ITEMS";
        private const string INSERT_INFOLINX_CLIENTS_PROC = "INSERT_INFOLINX_CLIENTS";
        private const string GET_INFOLINX_CLIENTS_PROC = "GET_INFOLINX_CLIENTS";
        private const string GET_GET_MAX_CLIENT_ID_PROC = "GET_MAX_CLIENT_ID";
        private const string IS_CONNECTION_UNIQUE_PROC = "IS_CONNECTION_UNIQUE";
        private const string DELETE_CONNECTION_BY_ID_PROC = "DELETE_CONNECTION_BY_ID";
        private const string GET_CONNECTIONS_FOR_CONN_TYPE_PROC = "GET_CONNECTIONS_FOR_CONN_TYPE";
        private const string INSERT_VR_SETTINGS_PROC = "INSERT_VR_SETTINGS";
        private const string UPDATE_VR_SETTINGS_PROC = "UPDATE_VR_SETTINGS";
        private const string GET_VR_SETTING_FOR_NAME_PROC = "GET_VR_SETTING_FOR_NAME";
        private const string GET_VR_SETTING_VALUE_FOR_NAME_PROC = "GET_VR_SETTING_VALUE_FOR_NAME";
        private const string GET_VR_SETTINGS_PROC = "GET_VR_SETTINGS";
        private const string UPDATE_NETWORK_CONNECTION_PROC = "UPDATE_NETWORK_CONNECTION";
        private const string GET_DELETED_INFOLINX_ITEMS_PROC = "GET_DELETED_INFOLINX_ITEMS";
        private const string UPDATE_DELETED_INFOLINX_ITEMS_PROC = "UPDATE_DELETED_INFOLINX_ITEMS";
        private const string GET_NETWORK_ITEMS_TO_ARCHIVE_OR_EXPUNGE_PROC = "GET_NETWORK_ITEMS_TO_ARCHIVE_OR_EXPUNGE";
        private const string UPDATE_NETWORK_IDS_PROC = "UPDATE_NETWORK_IDS";
        private const string GET_PATHS_FROM_NETWORK_ITEMS_PROC = "GET_PATHS_FROM_NETWORK_ITEMS";
        private const string GET_CONNECTION_NETWORK_ITEMS_FOR_CONNECTION_PROC = "GET_CONNECTION_NETWORK_ITEMS_FOR_CONNECTION";
        private const string DELETE_CONNECTION_NETWORK_ITEMS_FOR_ID_PROC = "DELETE_CONNECTION_NETWORK_ITEMS_FOR_ID";
        private const string UPDATE_CONNECTION_CATALOGUED_PROC = "UPDATE_CONNECTION_CATALOGUED";

        private const string GET_PATHS_AND_COUNT_FROM_NETWORK_ITEMS_PROC = "GET_PATHS_AND_COUNT_FROM_NETWORK_ITEMS";

        public static string INFOLINX_ITEMS_COL_I_ID = "I_ID";
        public static string INFOLINX_ITEMS_COL_BARCODE = "BARCODE";
        public static string INFOLINX_ITEMS_COL_QUICK_DESCRIPTION = "QUICK_DESCRIPTION";
        public static string INFOLINX_ITEMS_COL_I_IT_ID = "I_IT_ID";
        public static string INFOLINX_ITEMS_COL_FOLDER_NAME = "FOLDER_NAME";
        public static string INFOLINX_ITEMS_COL_CLIENT_ID = "CLIENT_ID";
        public static string INFOLINX_ITEMS_COL_PARENT_FOLDER = "PARENT_FOLDER";

        public static string NETWORK_COL_ID = "ID";
        public static string NETWORK_COL_I_ID = "I_ID";
        public static string NETWORK_COL_FOLDER_NAME = "FOLDER_NAME";
        public static string NETWORK_COL_FILE_NAME = "FILE_NAME";
        public static string NETWORK_COL_PATH = "PATH";
        public static string NETWORK_COL_RECORD_COUNT = "RECORD_COUNT";
        public static string NETWORK_COL_MATTER_ID = "MATTER_ID";
        public static string NETWORK_COL_UPDATE_INFOLINX = "UPDATE_INFOLINX";
        public static string NETWORK_COL_ON_DELETION = "ON_DELETION";

        public static string CONNECTION_COL_CONNECTION_NAME = "CONNECTION_NAME";
        public static string CONNECTION_COL_CONNECTION_TYPE = "CONNECTION_TYPE";
        public static string CONNECTION_COL_PATH = "PATH";
        public static string CONNECTION_COL_FILTER = "FILTER";
        public static string CONNECTION_COL_INDEX_CONTENT = "INDEX_CONTENT";
        public static string CONNECTION_COL_INCLUDE_SUB_DIRECTORIES = "INCLUDE_SUB_DIRECTORIES";
        public static string CONNECTION_COL_ARCHIVE = "ARCHIVE";
        public static string CONNECTION_COL_FILTER_INCLUDE = "FILTER_INCLUDE";
        public static string CONNECTION_COL_SEARCH_PATTERN = "SEARCH_PATTERN";
        public static string CONNECTION_COL_CATALOGUED = "CATALOGUED";

        public static string CONNECTION_NETWORK_ITEMS_TABLE = "CONNECTION_NETWORK_ITEMS";

        public static int ExternalTypeFileShare = 3;

        private string _connectionString;

        public DatabaseServices(string connectionString)
        {
            _connectionString = connectionString;
        }
        public static int _DeleteLogFileAfterDays = 3;
        public static int _Frequency = 3;
        public static int _MattersTab = 212;
        public static int _ItemTab = 218;
        public static int _DirectoryRefreshTime = 3;

        public static string _Password = "amyh";
        public static string _UserName = "";
        public static string _InfolinxColumnNames = "";
        public static string _InfolinxRestBaseAddress = "";
        public static string _MatterCode = "MATTER_CODE";
        public static string _ClientCode = "CLIENT_CODE";
        public static string _ArchiveLocation = "";
        public static string _ClientID = "FK_ITEM_CLIENT_ID";
        public static string _LogFilePath = "";


       
        public static bool VerboseLogging {get;set;}=true;
        public static int DeleteLogFileAfterDays { get; set; } = 3;
        public static int Frequency { get; set; } = 2;
        public static int MattersTab { get; set; } = 212;
        public static string ClientCode { get; set; } = "CLIENT_CODE";       
        public static string ClientID { get; set; } = "FK_ITEM_CLIENT_ID";
        public static int ItemTab { get; set; } = 218;
        public static int DirectoryRefreshTime { get; set; } = 3;
        public static string Password { get; set; } = "GJctQQwxJrRGMp6Rd4ImMw==";
        public static string UserName { get; set; } = "Superuser";
        public static string InfolinxColumnNames { get; set; } = "B_NAME,B_PATH,FK_ITEM_MATTER_ID,B_COUNT,FK_EXTERNAL_TYPE_ID";
        public static string InfolinxRestBaseAddress { get; set; } = "https://akingump.infolinx.com:81";        
        public static string MatterCode { get; set; } = "MATTER_CODE";
        public static string ArchiveLocation { get; set; } = Environment.CurrentDirectory;       
        public static string LogFilePath { get; set; } = System.IO.Path.Combine(Environment.CurrentDirectory, "log"); 
       
        public int UpdateConnectionCataloguedColumn(int ConnID)
        {           
            int intResult = -1;

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(UPDATE_CONNECTION_CATALOGUED_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", ConnID);
                cmd.Parameters.AddWithValue("@CATALOGUED", true);

                cn.Open();

                intResult = cmd.ExecuteNonQuery();                
            }

            return intResult;
        }
        public void RebuildIndexes()
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand("IX_REBUILD_INDEXES", cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 1000;

                cn.Open();

                object result = cmd.ExecuteScalar();
            }
        }

        public void BulkCopyItemIDs(DataTable objDt)
        {
            using (SqlBulkCopy bulk = new SqlBulkCopy(_connectionString, SqlBulkCopyOptions.CheckConstraints))
            {
                bulk.DestinationTableName = "ITEM_IDS";
                bulk.BulkCopyTimeout = 1000;
                bulk.BatchSize = 1000;
                bulk.WriteToServer(objDt);
            }
        }
        public int UpdateNetworkItemIDs()
        {
            //UPDATE_NETWORK_IDS
            int intResult = -1;

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(UPDATE_NETWORK_IDS_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 1000;
                cn.Open();

                object result = cmd.ExecuteNonQuery();
                if (result != null && result != DBNull.Value)
                {
                    intResult = Convert.ToInt32(result);
                }
            }

            return intResult;
        }
        public bool IsConnectionUnique(string connectionType, string path, string filter, bool indexContent)
        {
            bool isUnique = true;

            int intResult = 0;

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(IS_CONNECTION_UNIQUE_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CONNECTION_TYPE", connectionType);
                cmd.Parameters.AddWithValue("@PATH", path);
                cmd.Parameters.AddWithValue("@FILTER", filter);
                cmd.Parameters.AddWithValue("@INDEX_CONTENT", indexContent);

                cn.Open();

                object result = cmd.ExecuteScalar();

                if (result != null && result != DBNull.Value)
                {
                    result = Convert.ToInt32(result);
                }
            }
            
            if (intResult > 0)
            {
                isUnique = false;
            }

            return isUnique;
        }
        
        public DataTable GetInfolinxItems()
        {
            DataTable dt = new DataTable();

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_INFOLINX_ITEMS_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 1000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            return dt;
        }

        public int GetItemIdForFolderName(string folderName)
        {
            int id = -1;

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_ID_FOR_FOLDER_NAME_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FOLDER_NAME", folderName);

                cn.Open();

                object result = cmd.ExecuteScalar();
                if (result != null && result != DBNull.Value)
                {
                    id = Convert.ToInt32(result);
                }
            }

            return id;
        }

        public bool NetworkItemExists(string path, int count)
        {
            bool itemExists = false;

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_NETWORK_ITEM_FOR_PATH_AND_COUNT_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PATH", path);
                cmd.Parameters.AddWithValue("@RECORD_COUNT", count);

                cn.Open();

                object result = cmd.ExecuteScalar();
                if (result != null && result != DBNull.Value)
                {
                    itemExists = Convert.ToInt32(result) > 0;
                }
            }

            return itemExists;
        }

        public DataTable GetNetworkItemForPath(string path)
        {
            DataTable dt = new DataTable();

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_NETWORK_ITEM_FOR_PATH_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PATH", path);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            return dt;
        }

        public int DeleteConnection(int id)
        {
            int intResult = -1;

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(DELETE_CONNECTION_BY_ID_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", id);

                cn.Open();

                object result = cmd.ExecuteNonQuery();
                if (result != null && result != DBNull.Value)
                {
                    intResult = Convert.ToInt32(result);
                }
            }

            return intResult;
        }

        public bool DeleteNetworkItemByInfolinxId(int i_id)
        {
            bool success = false;

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(DELETE_NETWORK_ITEM_BY_INFOLINX_ID_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@I_ID", i_id);

                cn.Open();

                object result = cmd.ExecuteNonQuery();
                if (result != null && result != DBNull.Value)
                {
                    success = Convert.ToInt32(result) == 1;
                }
            }

            return success;
        }

        public DateTime? GetMaxModifyDate()
        {
            DateTime? date = null;

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_MAX_MODIFY_DATE_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();

                object result = cmd.ExecuteScalar();
                if (result != null && result != DBNull.Value)
                {
                    date = Convert.ToDateTime(result);
                }
            }

            return date;
        }

        public int GetMaxInfolinxItemId()
        {
            int id = -1;

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_MAX_ITEM_ID_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();

                object result = cmd.ExecuteScalar();
                if (result != null && result != DBNull.Value)
                {
                    id = Convert.ToInt32(result);
                }
            }

            return id;
        }
        
        public DataTable GetNetworkItems()
        {
            DataTable dt = new DataTable();

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_NETWORK_ITEMS_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            return dt;
        }

        public DataTable GetNetworkItemsToInsertOrUpdate()
        {
            DataTable dt = new DataTable();

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_NETWORK_ITEMS_TO_INSERT_OR_UPDATE_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            return dt;
        }

        /// <summary>
        /// Returns a data table from the view NETWORK_ITEMS_TO_DELETE_V,
        /// which is NETWORK_ITEMS joined to DELETED_INFOLINX_ITEMS.
        /// The ID column identifies a Network Item, and the DELETED_ID 
        /// column identifies a Deleted Item.
        /// </summary>
        /// <returns></returns>
        public DataTable GetNetworkItemsToArchiveOrDelete()
        {
            DataTable dt = new DataTable();

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_NETWORK_ITEMS_TO_ARCHIVE_OR_EXPUNGE_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            return dt;
        }

        public DataTable GetIndexedNetworkItems()
        {
            DataTable dt = new DataTable();

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_INDEXED_NETWORK_ITEMS_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            return dt;
        }

        public int InsertItemIntoNetworkTable(string path, int matterId, int count, string folderName, string fileName, string archiveOrDelete)
        {
            int id = 0;

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(INSERT_ITEMS_INTO_NETWORK_ITEMS_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@I_ID", DBNull.Value);
                cmd.Parameters.AddWithValue("@FOLDER_NAME", folderName);
                cmd.Parameters.AddWithValue("@FILE_NAME", fileName);
                cmd.Parameters.AddWithValue("@PATH", path);
                cmd.Parameters.AddWithValue("@RECORD_COUNT", count);
                cmd.Parameters.AddWithValue("@MATTER_ID", matterId);
                cmd.Parameters.AddWithValue("@ON_DELETION", archiveOrDelete);

                cn.Open();

                id = cmd.ExecuteNonQuery();
            }

            return id;
        }

        public int UpdateRecordCountInNetworkTable(int id, int count)
        {
            int result = 0;

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(UPDATE_ITEM_IN_NETWORK_ITEMS_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", id);
                cmd.Parameters.AddWithValue("@RECORD_COUNT", count);

                cn.Open();

                result = cmd.ExecuteNonQuery();
            }

            return result;
        }

        public int UpdateItemsNetworkTable(int localId, int infolinxId)
        {
            int result = 0;

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(UPDATE_ITEM_IN_LOCAL_DB_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", localId);
                cmd.Parameters.AddWithValue("@I_ID", infolinxId);

                cn.Open();

                result = cmd.ExecuteNonQuery();
            }

            return result;
        }

        public int RemoveUpdateFlagOnInfolinxItem(int itemId)
        {
            int result = 0;

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(UPDATE_ITEMS_REMOVE_UPD_FLAG_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@I_ID", itemId);

                cn.Open();

                result = cmd.ExecuteNonQuery();
            }

            return result;
        }

        public int UpdateMatterInNetworkTable(int localId, int matterId)
        {
            int result = 0;

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(UPDATE_MATTER_ID_IN_NETWORK_TABLE_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", localId);
                cmd.Parameters.AddWithValue("@I_ID", matterId);

                cn.Open();

                result = cmd.ExecuteNonQuery();
            }

            return result;
        }

        public int InsertNetworkRepositoryData(string connectionName, string connectionType, string path, string filter, bool indexContent, bool searchSubdirectories, bool archive, string filterInclude, string searchpattern)
        {
            int result = 0;

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(INSERT_NETWORK_CONNECTION, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CONNECTION_NAME", connectionName);
                cmd.Parameters.AddWithValue("@CONNECTION_TYPE", connectionType);
                cmd.Parameters.AddWithValue("@PATH", path);
                cmd.Parameters.AddWithValue("@FILTER", filter);
                cmd.Parameters.AddWithValue("@INDEX_CONTENT", indexContent);
                cmd.Parameters.AddWithValue("@INCLUDE_SUB_DIRECTORIES", searchSubdirectories);
                cmd.Parameters.AddWithValue("@ARCHIVE", archive);
                cmd.Parameters.AddWithValue("@FILTER_INCLUDE", filterInclude);
                cmd.Parameters.AddWithValue("@SEARCH_PATTERN", searchpattern);
                cmd.Parameters.AddWithValue("@CATALOGUED", false);

                cn.Open();

                result = cmd.ExecuteNonQuery();
            }

            return result;
        }

        public int UpdateNetworkRepositoryData(int ID, string connectionName, string connectionType, string path, string filter, bool indexContent, bool searchSubdirectories, bool archive, string filterInclude, string searchpattern)
        {
            int result = 0;

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(UPDATE_NETWORK_CONNECTION_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", ID);
                cmd.Parameters.AddWithValue("@CONNECTION_NAME", connectionName);
                cmd.Parameters.AddWithValue("@CONNECTION_TYPE", connectionType);
                cmd.Parameters.AddWithValue("@PATH", path);
                cmd.Parameters.AddWithValue("@FILTER", filter);
                cmd.Parameters.AddWithValue("@INDEX_CONTENT", indexContent);
                cmd.Parameters.AddWithValue("@INCLUDE_SUB_DIRECTORIES", searchSubdirectories);
                cmd.Parameters.AddWithValue("@ARCHIVE", archive);
                cmd.Parameters.AddWithValue("@FILTER_INCLUDE", filterInclude);
                cmd.Parameters.AddWithValue("@SEARCH_PATTERN", searchpattern);

                cn.Open();

                result = cmd.ExecuteNonQuery();
            }

            return result;
        }

        protected DataTable BuildConnectionsDataTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", typeof(int));
            dt.Columns.Add("CONNECTION_NAME", typeof(String));
            dt.Columns.Add("CONNECTION_TYPE", typeof(String));
            dt.Columns.Add("PATH", typeof(String));
            dt.Columns.Add("FILTER", typeof(String));
            dt.Columns.Add("INDEX_CONTENT", typeof(bool));
            dt.Columns.Add("INCLUDE_SUB_DIRECTORIES", typeof(bool));
            dt.Columns.Add("ARCHIVE", typeof(bool));
            dt.Columns.Add("FILTER_INCLUDE", typeof(String));
            dt.Columns.Add("SEARCH_PATTERN", typeof(String));
            dt.Columns.Add("CATALOGUED", typeof(bool));
            dt.PrimaryKey = new DataColumn[] { dt.Columns["ID"] };

            return dt;
        }

        public DataTable GetConnections()
        {
            DataTable dt = BuildConnectionsDataTable();

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_CONNECTIONS, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            return dt;
        }

        public DataTable GetConnectionsForConnectionType(string type)
        {
            DataTable dt = BuildConnectionsDataTable();

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_CONNECTIONS_FOR_CONN_TYPE_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CONNECTION_TYPE", type);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            return dt;
        }

        public int InsertIntoInfolinxItemsTable(int itemId, string barcode, string quickDescription, int itemTypeId, string folderName, int clientId, string parentFolder)
        {
            int result = 0;

            try
            {
                using (SqlConnection cn = new SqlConnection(_connectionString))
                using (SqlCommand cmd = new SqlCommand(INSERT_INFOLINX_ITEMS_PROC, cn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@I_ID", itemId);
                    cmd.Parameters.AddWithValue("@BARCODE", barcode);
                    cmd.Parameters.AddWithValue("@QUICK_DESCRIPTION", quickDescription);
                    cmd.Parameters.AddWithValue("@I_IT_ID", itemTypeId);
                    cmd.Parameters.AddWithValue("@FOLDER_NAME", folderName);
                    cmd.Parameters.AddWithValue("@CLIENT_ID", clientId);
                    cmd.Parameters.AddWithValue("@PARENT_FOLDER", parentFolder);

                    cn.Open();

                    result = cmd.ExecuteNonQuery();
                }
            }
            catch
            {
                //Unique index on i_id ?
            }

            return result;
        }

        /// <summary>
        /// Insert into deleted items table (i.e. items that have been deleted in Infolinx)
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="deletedDate"></param>
        /// <returns></returns>
        public int InsertIntoDeletedItemsTable(int itemId, DateTime deletedDate)
        {
            int result = 0;

            try
            {
                using (SqlConnection cn = new SqlConnection(_connectionString))
                using (SqlCommand cmd = new SqlCommand(INSERT_DELETED_INFOLINX_ITEMS_PROC, cn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@I_ID", itemId);
                    cmd.Parameters.AddWithValue("@MODIFY_DATE", deletedDate);

                    cn.Open();

                    result = cmd.ExecuteNonQuery();
                }
            }
            catch
            {
                //i_id unique index
            }

            return result;
        }

        /// <summary>
        /// Update table with the result of trying to delete or archive items on the network
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public int UpdateDeletedItemsTable(int itemId, string result)
        {
            int intRet = 0;

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(UPDATE_DELETED_INFOLINX_ITEMS_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@I_ID", itemId);
                cmd.Parameters.AddWithValue("@RESULT", result);

                cn.Open();

                intRet = cmd.ExecuteNonQuery();
            }

            return intRet;
        }

        /// <summary>
        /// These are items that have been deleted in Infolinx
        /// </summary>
        /// <returns></returns>
        public DataTable GetDeletedItems()
        {
            DataTable dt = new DataTable();

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_DELETED_INFOLINX_ITEMS_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            return dt;
        }

        public int InsertVRSettings(string settingName, string dataType, string settingScope, string settingValue)
        {
            int id = 0;

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(INSERT_VR_SETTINGS_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SETTING_NAME", settingName);
                cmd.Parameters.AddWithValue("@DATA_TYPE", dataType);
                cmd.Parameters.AddWithValue("@SETTING_SCOPE", settingScope);
                cmd.Parameters.AddWithValue("@SETTING_VALUE", settingValue);
              
                cn.Open();

                id = cmd.ExecuteNonQuery();
            }

            return id;
        }

        public int UpdateVRSettings(string settingName, string dataType, string settingScope, string settingValue)
        {
            int id = 0;

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(UPDATE_VR_SETTINGS_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SETTING_NAME", settingName);
                cmd.Parameters.AddWithValue("@DATA_TYPE", dataType);
                cmd.Parameters.AddWithValue("@SETTING_SCOPE", settingScope);
                cmd.Parameters.AddWithValue("@SETTING_VALUE", settingValue);

                cn.Open();

                id = cmd.ExecuteNonQuery();
            }

            return id;
        }

        /// <summary>
        /// Get all settings from database
        /// </summary>
        /// <returns></returns>
        public DataTable GetVRSettings()
        {
            DataTable dt = new DataTable();

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_VR_SETTINGS_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            return dt;
        }

        /// <summary>
        /// Get one setting (which one?)
        /// </summary>
        /// <returns></returns>
        public DataTable GetVRSetting()
        {
            DataTable dt = new DataTable();

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_VR_SETTING_FOR_NAME_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            return dt;
        }

        public string GetVRSettingForName(string settingName)
        {
            string value = "";

            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_VR_SETTING_VALUE_FOR_NAME_PROC, cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SETTING_NAME", settingName);
                cn.Open();

                object result = cmd.ExecuteScalar();
                if (result != null && result != DBNull.Value)
                {
                    value = result.ToString();
                }
            }

            return value;
        }

        public void UpdateHangFireJobTable()
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand("UPDATE_HANGFIRE_JOB", cn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();

                cmd.ExecuteNonQuery();
            }
        }

        public static bool TestConnection()
        {
            bool connectionSucceeded;

            try
            {
                SqlConnection cn = new SqlConnection(Properties.Settings.Default.ConnectionString);
                cn.Open();
                cn.Close();

                connectionSucceeded = true;
            }
            catch
            {
                connectionSucceeded = false;
            }

            return connectionSucceeded;
        }
        public DataTable GetExistingNetworkItemPaths()
        {
            DataTable objDT = new DataTable();
            objDT.Columns.Add("PATH", typeof(String));
            objDT.PrimaryKey = new DataColumn[] { objDT.Columns["PATH"] };

            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(GET_PATHS_FROM_NETWORK_ITEMS_PROC, cn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 1000;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);

                    da.Fill(objDT);
                }
            }

            return objDT;
        }

        public DataTable GetExistingNetworkItemPathsAndRecordCount()
        {
            DataTable objDT = new DataTable();
            objDT.Columns.Add("ID", typeof(Int32));
            objDT.Columns.Add("PATH", typeof(String));
            objDT.Columns.Add("RECORD_COUNT", typeof(Int32));

            objDT.PrimaryKey = new DataColumn[] { objDT.Columns["PATH"]};

            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(GET_PATHS_AND_COUNT_FROM_NETWORK_ITEMS_PROC, cn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 1000;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);

                    da.Fill(objDT);
                }
            }

            return objDT;
        }



        /// <summary>
        /// deletes from CONNECTION_NETWORK_ITEMS table where the connection id  =  the id passed in
        /// </summary>
        /// <param name="ConnId"></param>
        public void ClearConnectionNetworkItemsForConnection(int ConnId)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(DELETE_CONNECTION_NETWORK_ITEMS_FOR_ID_PROC, cn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CONNECTION_ID", ConnId);
                    cmd.CommandTimeout = 1000;

                    cn.Open();

                    object result = cmd.ExecuteScalar();
                }
            }
        }
        public DataTable GetRowsFromConnectionNetworkItemsForConnection(int ConnID)
        {
            DataTable dt = new DataTable();

            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(GET_CONNECTION_NETWORK_ITEMS_FOR_CONNECTION_PROC, cn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 1000;
                    cmd.Parameters.AddWithValue("@CONNECTION_ID", ConnID);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                }
            }

            return dt;
        }
    }
}
