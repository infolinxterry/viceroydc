﻿using Hangfire;
using Hangfire.Storage;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ViceroyService
{
    public class FileShareAgent
    {
        private DatabaseServices _ds;
        private InfolinxItem _item;
        private Logger _logger;

        protected string RunningJobID { get; set; }

        public FileShareAgent()
        {
            _ds = new DatabaseServices(Properties.Settings.Default.ConnectionString);
            _item = new InfolinxItem();
            _logger = new Logger();

            RunningJobID = "1";
        }

        public FileShareAgent(DatabaseServices databaseServices, InfolinxItem item, Logger logger)
        {
            _ds = databaseServices;
            _item = item;
            _logger = new Logger();

            RunningJobID = "1";
        }
        public void CatalogDirectories(DataTable connsDT)
        {            
            if (this.dicDirSearch != null)
            {
                this.dicDirSearch.Clear();
            }
            else
            {
                this.dicDirSearch = new Dictionary<int, bool>();
            }

            this.dicDirSearch.Add(DateTime.Now.Day, true);

            foreach (DataRow connRow in connsDT.Rows)
            {
                int ConnectionID = Convert.ToInt32(connRow["ID"]);
                string strPath = connRow[DatabaseServices.CONNECTION_COL_PATH].ToString();
                string strSearchPattern = connRow[DatabaseServices.CONNECTION_COL_SEARCH_PATTERN].ToString();

                List<Matter> lstMatters = this.GetValidMattersList();
                this.InsertIntoConnectionNetworkItemsTable(ConnectionID, strPath, strSearchPattern, lstMatters);

                _ds.UpdateConnectionCataloguedColumn(ConnectionID);
            }
        }
        Dictionary<int, bool> dicDirSearch { get; set; }
        public async Task SynchronizeContent()
        {
            _logger.LogMessageToFile("Data gathering from File Share - starting");
       
            // Get matters from web service and insert into local db
            await _item.GetInfolinxMatters();
           
            //see if we need to catalog any connections 
            DataTable connsDT = _ds.GetConnections();
            bool EveryConnectionNeedsToBeCatalogued = true;

            try
            {
                foreach (DataRow connRow in connsDT.Rows)
                {
                    bool Catalogued = Convert.ToBoolean(connRow[DatabaseServices.CONNECTION_COL_CATALOGUED]);
                    if (!Catalogued)
                    {
                        int ConnectionID = Convert.ToInt32(connRow["ID"]);
                        string strPath = connRow[DatabaseServices.CONNECTION_COL_PATH].ToString();
                        string strSearchPattern = connRow[DatabaseServices.CONNECTION_COL_SEARCH_PATTERN].ToString();

                        List<Matter> lstMatters = this.GetValidMattersList();
                        this.InsertIntoConnectionNetworkItemsTable(ConnectionID, strPath, strSearchPattern, lstMatters);

                        _ds.UpdateConnectionCataloguedColumn(ConnectionID);
                    }
                    else
                    {
                        EveryConnectionNeedsToBeCatalogued = false;
                    }
                }
            }
            catch(Exception ex)
            {
                _logger.LogException("SynchronizeContent", ex);
            }

            if(EveryConnectionNeedsToBeCatalogued)
            {
                this.dicDirSearch = new Dictionary<int, bool>();
                this.dicDirSearch.Add(DateTime.Now.Day, true);
            }

            int HourToRunDirSearch = DatabaseServices.DirectoryRefreshTime; 

            DateTime dtToRunDirSeach = DateTime.Now.Date.AddHours(HourToRunDirSearch);

            if (DateTime.Now.CompareTo(dtToRunDirSeach) >= 0)
            {
                bool RunSearch = true;
                if (this.dicDirSearch != null)
                {
                    foreach (KeyValuePair<int, bool> vals in this.dicDirSearch)
                    {
                        if (vals.Key == DateTime.Now.Day && vals.Value == true)
                        {
                            RunSearch = false;
                        }
                    }
                }
                if (RunSearch)
                {
                    if (DatabaseServices.VerboseLogging)
                    {
                        _logger.LogMessageToFile("About to catalog directories for matters");
                    }

                    this.CatalogDirectories(connsDT);
                }
            }

            var watch = Stopwatch.StartNew();

            if (DatabaseServices.VerboseLogging)
            {
                _logger.LogMessageToFile("About to call SynchronizeFileShareWithLocalDb");
            }

            SynchronizeFileShareWithLocalDb();

            if (DatabaseServices.VerboseLogging)
            {
                _logger.LogMessageToFile("About to call SynchronizeLocalDbWithInfolinx()");
            }

            await SynchronizeLocalDbWithInfolinx();

            if (DatabaseServices.VerboseLogging)
            {
                _logger.LogMessageToFile("About to call GetDeletedItemsInInfolinx()");
            }
            // Get deleted items from Infolinx, and put in deleted items table in local db           
            await GetDeletedItemsInInfolinx();
            if (DatabaseServices.VerboseLogging)
            {
                _logger.LogMessageToFile("About to call DealWithNetworkItems()");
            }
            //deal the items on the network
            List<int> lstItemsToDelete = this.DealWithNetworkItems();

            if (DatabaseServices.VerboseLogging)
            {
                _logger.LogMessageToFile("About to call DeleteItemsInNetworkItemsTable()");
            }
            //delete them out of the networks items table
            this.DeleteItemsInNetworkItemsTable(lstItemsToDelete);

            _logger.LogMessageToFile("Data operations finished in SynchronizeContent()");
            _logger = new Logger();

            DeleteOldLogFiles();

            //this will rebuild local indexes if the fragmentation is over 20 for any table
            if (DatabaseServices.VerboseLogging)
            {
                _logger.LogMessageToFile("About to call RebuildIndexes()");
            }

            _ds.RebuildIndexes();

            ScheduleNextJob(watch);            
        }
            
        private List<int> DealWithNetworkItems()
        {
            DataTable objDT = _ds.GetNetworkItemsToArchiveOrDelete();
            List<int> lstItemsToDelete = new List<int>();
            string strArchiveLocation = DatabaseServices.ArchiveLocation;
            if(!strArchiveLocation.EndsWith("\\"))
            {
                strArchiveLocation += "\\";
            }

            foreach (DataRow row in objDT.Rows)
            {
                bool Delete = row["ON_DELETION"].ToString().ToUpper() == "DELETE";

                if (row["FILE_NAME"] != DBNull.Value) //we are indexing every file
                {
                    if (File.Exists(row["PATH"].ToString()))
                    {
                        try
                        {
                            if (Delete)
                            {
                                File.Delete(row["PATH"].ToString());
                            }
                            else
                            {
                                File.Move(row["PATH"].ToString(), strArchiveLocation + row["FILE_NAME"].ToString());
                            }

                            lstItemsToDelete.Add(Convert.ToInt32(row["I_ID"]));
                            _ds.UpdateDeletedItemsTable(Convert.ToInt32(row["DELETED_ID"]), "Success");
                        }
                        catch (Exception ex)
                        {
                            _logger.LogException("Error trying to " + row["ON_DELETION"].ToString() + " file " + row["PATH"].ToString(), ex);
                            _ds.UpdateDeletedItemsTable(Convert.ToInt32(row["DELETED_ID"]), ex.Message);
                        }
                    }
                    else
                    {
                        //file no longer exists just delete this out of network items table
                        lstItemsToDelete.Add(Convert.ToInt32(row["I_ID"]));
                        _ds.UpdateDeletedItemsTable(Convert.ToInt32(row["DELETED_ID"]), "Success");
                    }
                }
                else //we are looking at directories
                {
                    //Delete all files in directory
                    if (Directory.Exists(row["PATH"].ToString()))
                    {
                        try
                        {
                            DirectoryInfo di = new DirectoryInfo(row["PATH"].ToString());

                            foreach (FileInfo file in di.GetFiles())
                            {
                                if (Delete)
                                {
                                    file.Delete();
                                }
                                else
                                {
                                    file.MoveTo(strArchiveLocation + file.Name);
                                }
                            }
                            lstItemsToDelete.Add(Convert.ToInt32(row["ID"]));
                            _ds.UpdateDeletedItemsTable(Convert.ToInt32(row["DELETED_ID"]), "Success");
                        }
                        catch (Exception ex)
                        {
                            _logger.LogException("Error trying to " + row["ON_DELETION"].ToString() + " file " + row["PATH"].ToString(), ex);
                            _ds.UpdateDeletedItemsTable(Convert.ToInt32(row["DELETED_ID"]), ex.Message);
                        }
                    }
                    else
                    {
                        //directory no longer exists just delete out of table
                        lstItemsToDelete.Add(Convert.ToInt32(row["ID"]));
                        _ds.UpdateDeletedItemsTable(Convert.ToInt32(row["DELETED_ID"]), "Success");
                    }
                }
            }          

            return lstItemsToDelete;
        }
        private void DeleteItemsInNetworkItemsTable(List<int> lstItems)
        {
            foreach(int id in lstItems)
            {
                _ds.DeleteNetworkItemByInfolinxId(id);
            }
        }
        private async Task GetDeletedItemsInInfolinx()
        {
            List<IRSimpleItem> lstSimp = await _item.GetDeletedItemsFromInfolinx();

            foreach (IRSimpleItem item in lstSimp)
            {
                try
                {
                    _ds.InsertIntoDeletedItemsTable(item.I, Convert.ToDateTime(item.R));
                }
                catch(Exception ex)
                {
                    //may get duplicate items 
                    _logger.LogException("Error inserting item into deleted items table ", ex);
                    
                }
            }
        }

        private void ScheduleNextJob(Stopwatch watch)
        {
            long elapsedMs = watch.ElapsedMilliseconds;

            int elapsedMinutes = Convert.ToInt32(Math.Round(Convert.ToDecimal((elapsedMs / 1000) / 60)));

            //don't queue up another job if the elspsed time is shorter than the interval
            int Interval = DatabaseServices.Frequency * 60;

            if (elapsedMinutes < Interval)
            {
                Interval = Interval - elapsedMinutes;
                var JobID = BackgroundJob.Schedule(() => SynchronizeContent(), TimeSpan.FromMinutes(Convert.ToDouble(Interval)));
            }
            else
            {
                var JobID = BackgroundJob.Schedule(() => SynchronizeContent(), TimeSpan.Zero);
            }
        }

        private void DeleteOldLogFiles()
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(DatabaseServices.LogFilePath);

                if (di.Exists)
                {
                    int intDays = DatabaseServices.DeleteLogFileAfterDays;
                    intDays = intDays * -1;

                    foreach (FileInfo fi in di.GetFiles("*.log"))
                    {
                        if (fi.Name.StartsWith("VR_"))
                        {
                            if (fi.CreationTime < DateTime.Now.AddMonths(intDays))
                            {
                                fi.Delete();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogException("Error deleting old log files", ex);
            }
        }

        private async Task SynchronizeLocalDbWithInfolinx()
        {
            if (DatabaseServices.VerboseLogging)
            {
                _logger.LogMessageToFile("Start of SynchronizeLocalDbWithInfolinx");
            }
           
            // Get files and directory/count pairs (collectively termed network items)
            DataTable networkItemsDT = _ds.GetNetworkItems();

            List<IxItem> lstItemsToCreate = new List<IxItem>();
            List<IxItem> lstItemsToUpdate = new List<IxItem>();

            // For each item
            foreach (DataRow row in networkItemsDT.Rows)
            {
                // If the item does not exist in Infolinx
                if (row[DatabaseServices.NETWORK_COL_I_ID] == DBNull.Value)
                {
                    if (IndexingContent(row))
                    {
                       // if (File.Exists(row["PATH"].ToString())) //the only reason to check this would be if they deleted the file right after we put it in the db
                       // {                            
                            this.CreateNetworkItemRowInInfolinx(row, lstItemsToCreate);
                       // }
                    }
                    else
                    {
                        //if(Directory.Exists(row["PATH"].ToString()))
                        //{
                            this.CreateNetworkItemRowInInfolinx(row, lstItemsToCreate);
                        //}
                    }
                }
                else
                {
                    // If the count is 0 or (indexing content and file doesn't exist)
                    if (Convert.ToInt32(row[DatabaseServices.NETWORK_COL_RECORD_COUNT]) == 0 || (IndexingContent(row) && !File.Exists(row["PATH"].ToString())))
                    {
                        // Delete the item in Infolinx
                        await _item.DeleteItemInInfolinx(Convert.ToInt32(row["I_ID"]));
                    }
                    // If we are not indexing
                    else if (!IndexingContent(row))
                    {
                        if (row[DatabaseServices.NETWORK_COL_UPDATE_INFOLINX] != DBNull.Value && Convert.ToBoolean(row[DatabaseServices.NETWORK_COL_UPDATE_INFOLINX]))
                        {
                            lstItemsToUpdate.Add(new IxItem(Convert.ToInt32(row["I_ID"]), Convert.ToInt32(row[DatabaseServices.NETWORK_COL_RECORD_COUNT]), Convert.ToInt32(row[DatabaseServices.NETWORK_COL_ID])));
                        }
                    }
                }
            }

            if (DatabaseServices.VerboseLogging)
            {
                _logger.LogMessageToFile("about to call CreateItemsInInfolinx with " + lstItemsToCreate.Count.ToString() + " items");
            }

            if (lstItemsToCreate.Count > 0)
            {
                await _item.CreateItemsInInfolinx(lstItemsToCreate);
            }
            if(lstItemsToUpdate.Count > 0)
            {
                if (DatabaseServices.VerboseLogging)
                {
                    _logger.LogMessageToFile("about to call UpdateItemsInInfolinx with " + lstItemsToUpdate.Count.ToString() + " items");
                }

                await _item.UpdateItemsInInfolinx(lstItemsToUpdate);
            }
        }

        private void CreateNetworkItemRowInInfolinx(DataRow row, List<IxItem> lstItemsToCreate)
        {
            string path = row[DatabaseServices.NETWORK_COL_PATH].ToString();
            int recordCount = row[DatabaseServices.NETWORK_COL_RECORD_COUNT] != DBNull.Value ?
                Convert.ToInt32(row[DatabaseServices.NETWORK_COL_RECORD_COUNT]) : 1;
            int id = Convert.ToInt32(row[DatabaseServices.NETWORK_COL_ID]);
            //int externalType = 3;
            string folderName = row[DatabaseServices.NETWORK_COL_FOLDER_NAME].ToString();
            string fileName = row[DatabaseServices.NETWORK_COL_FILE_NAME].ToString();
            int matterId = Convert.ToInt32(row[DatabaseServices.NETWORK_COL_MATTER_ID]);

            // Create the item in Infolinx
            // await _item.CreateItemInInfolinx(path, recordCount, id, externalType, folderName, fileName, matterId);
            IxItem item = new IxItem(path, recordCount, id, DatabaseServices.ExternalTypeFileShare, folderName, fileName, matterId);
            lstItemsToCreate.Add(item);            
        }

        private bool IndexingContent(DataRow row)
        {
            return row[DatabaseServices.NETWORK_COL_FILE_NAME] != DBNull.Value && 
                row[DatabaseServices.NETWORK_COL_FILE_NAME].ToString() != string.Empty;
        }

        private void SynchronizeFileShareWithLocalDb()
        {
            // Get connections from local db
            DataTable connsDT = _ds.GetConnections();

            DataTable netWorkItemsDT = null;


            foreach (DataRow connRow in connsDT.Rows)
            {
                int ConnectionID = Convert.ToInt32(connRow["ID"]);
                string strPath = connRow[DatabaseServices.CONNECTION_COL_PATH].ToString();
                string strSearchPattern = connRow[DatabaseServices.CONNECTION_COL_SEARCH_PATTERN].ToString();
                string OnDeletion = Convert.ToBoolean(connRow[DatabaseServices.CONNECTION_COL_ARCHIVE]) == false ? "Delete" : "Archive";
                string strFilter = connRow[DatabaseServices.CONNECTION_COL_FILTER].ToString();
                string strFilterInclude = connRow[DatabaseServices.CONNECTION_COL_FILTER_INCLUDE].ToString();
                bool IndexContent = Convert.ToBoolean(connRow[DatabaseServices.CONNECTION_COL_INDEX_CONTENT]);

                if(IndexContent)
                {
                    netWorkItemsDT = _ds.GetExistingNetworkItemPaths();
                }
                else
                {
                    netWorkItemsDT= _ds.GetExistingNetworkItemPathsAndRecordCount();
                }
                //now go through the CONNECTION_NETWORK_ITEMS table and use the PHYSICAL_PATH column to find files
                this.InsertFilesforIndexing(ConnectionID, strFilter, strFilterInclude, OnDeletion, IndexContent, netWorkItemsDT);
               
            }

            connsDT.Dispose();
            connsDT = null;
           
        }

        private void InsertDirectoryCountPairsIntoLocalDb(DataRow matter, DataRow connRow)
        {
            // Get directories and counts of folders matching matter name
            string strSearchPattern = connRow[DatabaseServices.CONNECTION_COL_SEARCH_PATTERN].ToString();
            bool IncludeSubdirectories = Convert.ToBoolean(connRow[DatabaseServices.CONNECTION_COL_INCLUDE_SUB_DIRECTORIES]);
            string strFilter = connRow[DatabaseServices.CONNECTION_COL_FILTER].ToString();
            string strIncludeOrExclude = connRow[DatabaseServices.CONNECTION_COL_FILTER_INCLUDE].ToString();

            string ParentFolderToFind = strSearchPattern != String.Empty ? strSearchPattern.Replace("%", matter["PARENT_FOLDER"].ToString()) : matter["PARENT_FOLDER"].ToString(); //this will give us a string like *Folder Name or *FolderName* or FolderName* or FolderName
            
            string ChildFolder = strSearchPattern != String.Empty ? strSearchPattern.Replace("%", matter["FOLDER_NAME"].ToString()) : matter["FOLDER_NAME"].ToString();

            IEnumerable<string> lstClientFolders = this.GetDirectoriesMatchingMatterName(connRow["PATH"].ToString(), ParentFolderToFind, IncludeSubdirectories);
            
            foreach (string clientFolder in lstClientFolders)
            {                
                Dictionary<string, int> dictFiles = GetFileCountsFromDirectoriesMatchingMatterName(clientFolder, ChildFolder,strFilter, IncludeSubdirectories, strIncludeOrExclude);

                // For each directory/count pair
                foreach (KeyValuePair<string, int> kvp in dictFiles)
                {
                    // Get record matching pair (id, count)
                    DataTable dt = _ds.GetNetworkItemForPath(kvp.Key); //3 columns ID, RECORD_COUNT,I_ID                    

                    if (dt.Rows.Count > 0)
                    {                       
                        // If the count is different
                        if (Convert.ToInt32(dt.Rows[0][1]) != kvp.Value)
                        {
                            // Update the count
                            _ds.UpdateRecordCountInNetworkTable(Convert.ToInt32(dt.Rows[0][0]), kvp.Value);
                        }                        
                    }
                    else
                    {
                        if (kvp.Value > 0) //only insert if we have files 
                        {
                            string strFolderName = Path.GetFileName(kvp.Key);

                            // Insert the pair
                            _ds.InsertItemIntoNetworkTable(kvp.Key, Convert.ToInt32(matter["I_ID"]), kvp.Value, strFolderName, "", connRow[DatabaseServices.CONNECTION_COL_ARCHIVE].ToString());
                        }
                    }
                }
            }
        }

        private void InsertFileNamesIntoLocalDb(DataRow matter, DataRow connRow)
        {
            // Get all files matching matter name
            //must take into account the search pattern
            string strSearchPattern = connRow[DatabaseServices.CONNECTION_COL_SEARCH_PATTERN].ToString();
            string strIncludeOrExclude = connRow[DatabaseServices.CONNECTION_COL_FILTER_INCLUDE].ToString();
            bool blnIncludeSubdirectoryies = Convert.ToBoolean(connRow[DatabaseServices.CONNECTION_COL_INCLUDE_SUB_DIRECTORIES]);
            string strFilter = connRow[DatabaseServices.CONNECTION_COL_FILTER].ToString();

            string ParentFolderToFind = strSearchPattern != String.Empty ? strSearchPattern.Replace("%", matter["PARENT_FOLDER"].ToString()) : matter["PARENT_FOLDER"].ToString(); //this will give us a string like *Folder Name or *FolderName* or FolderName* or FolderName            

            IEnumerable <string> MatchingClientFolders = this.GetDirectoriesMatchingMatterName(connRow["PATH"].ToString(), ParentFolderToFind, blnIncludeSubdirectoryies);

            foreach (string ClientFolder in MatchingClientFolders)
            {
                string ChildFolder = strSearchPattern != String.Empty ? strSearchPattern.Replace("%", matter["FOLDER_NAME"].ToString()) : matter["FOLDER_NAME"].ToString();

                List<string> lstFiles = FindFilesInDirectoriesMatchingMatterName(ClientFolder, ChildFolder, strFilter, strIncludeOrExclude, blnIncludeSubdirectoryies);

                foreach (string strPath in lstFiles)
                {
                    // If file does not exist in local db
                    if (!_ds.NetworkItemExists(strPath, 1))
                    {
                        DirectoryInfo di = new DirectoryInfo(strPath);
                        string strFolderName = di.Parent.ToString();
                        string strFileName = Path.GetFileName(strPath);

                        // Insert file
                        _ds.InsertItemIntoNetworkTable(strPath, Convert.ToInt32(matter["I_ID"]), 1, strFolderName, strFileName, connRow[DatabaseServices.CONNECTION_COL_ARCHIVE].ToString());
                    }
                }

            }
        }

        private DataTable GetValidMatters()
        {
            // Get matters from local db
            DataTable mattersDT = _ds.GetInfolinxItems();
            char[] badChars = Path.GetInvalidPathChars();
            List<string> lstBad = new List<string>();
            lstBad.Add("/");
            foreach (var c in badChars)
            {
                lstBad.Add(c.ToString());
            }

            List<DataRow> lstRowsToRemove = new List<DataRow>();

            foreach (DataRow matterRow in mattersDT.Rows)
            {
                foreach (string c in lstBad)
                {                    
                    if (matterRow["FOLDER_NAME"].ToString().Contains(c) || matterRow["PARENT_FOLDER"].ToString().Contains(c))
                    {
                        //mattersDT.Rows.Remove(matterRow);
                        lstRowsToRemove.Add(matterRow);
                    }
                }
            }

            foreach (DataRow row in lstRowsToRemove)
            {
                mattersDT.Rows.Remove(row);
                mattersDT.AcceptChanges();
            }

            return mattersDT;
        }

        public List<string> FindFilesInDirectoriesMatchingMatterName(string baseDirectory, string matterName, string Filter, string IncludeOrExclude, bool IncludeSubdirectories)
        {
            List<string> files = new List<string>();

            IEnumerable<string> directories = this.GetDirectoriesMatchingMatterName(baseDirectory, matterName, IncludeSubdirectories);

            string[] filters = Filter.Split(',');

            foreach (var directory in directories)
            {
                //if filter is exclude must get all files then excluded files                              
                if (IncludeOrExclude == "Exclude")
                {                   
                    var allFiles = Directory.GetFiles(directory, "*.*");
                    var filesToexclude = filters.SelectMany(f => Directory.GetFiles(directory, f).ToArray());
                    files.AddRange(allFiles.Except(filesToexclude));
                }
                else //filter is include
                {                   
                    files.AddRange(filters.SelectMany(f => Directory.GetFiles(directory, f).ToArray()));
                }
            }

            return files;
        }

        public Dictionary<string, int> GetFileCountsFromDirectoriesMatchingMatterName(string baseDirectory, string matterName, string Filter, bool IncludeSubDirectories, string IncludeOrExclude)
        {
            Dictionary<string, int> counts = new Dictionary<string, int>();
            
            IEnumerable<string> directories = this.GetDirectoriesMatchingMatterName(baseDirectory, matterName, IncludeSubDirectories);

            string[] filters = Filter.Split(',');
            List<string> files = new List<string>();

            foreach (var directory in directories)
            {
                if (IncludeOrExclude == "Exclude")
                {
                    var allFiles = Directory.GetFiles(directory, "*.*");
                    var filesToexclude = filters.SelectMany(f => Directory.GetFiles(directory, f).ToArray());
                    files.AddRange(allFiles.Except(filesToexclude));
                    counts.Add(directory, files.Count);
                }
                else
                {
                    files.AddRange(filters.SelectMany(f => Directory.GetFiles(directory, f).ToArray()));
                    counts.Add(directory, files.Count);
                }
            }

            return counts;
        }

        public string[] GetAllDirectories(string baseDirectory, string matterName, List<string> directories)
        {
            string[] subdirectories = null;

            subdirectories = Directory.GetDirectories(baseDirectory, matterName);

            foreach (string dir in subdirectories)
            {
                directories.Add(dir);
                GetAllDirectories(dir, matterName, directories);
            }

            return directories.ToArray();
        }

        public IEnumerable<string> GetDirectoriesMatchingMatterName(string baseDirectory, string matterName,bool IncludeSubdirectories)
        {
            List<string> directories = new List<string>();

            if (!Directory.Exists(baseDirectory))
            {
                return directories;
            }
           
            if (baseDirectory.Contains(matterName))
            {
                directories.Add(baseDirectory);
            }

            string[] subdirectories = null;

            if (IncludeSubdirectories)
            {
                try
                {
                    subdirectories = Directory.GetDirectories(baseDirectory, matterName, SearchOption.AllDirectories);
                }
                catch
                {
                    //must use recursive funtion then
                    try
                    {
                        List<string> lstDirs = new List<string>();
                        this.GetAllDirectories(baseDirectory, matterName, lstDirs);
                    }
                    catch(Exception ex) //don't kill the searching if we can search all subdirectories - just log error
                    {                       
                        _logger.LogException("GetDirectoriesMatchingMatterName", ex);
                    }
                }
            }
            else
            {
                subdirectories = Directory.GetDirectories(baseDirectory, matterName);
            }

            directories.AddRange(subdirectories);

            return directories;
        }

        // Not sure if we need the methods below...

        public void RemoveOldHangFireJobs(string strJobID)
        {
            try
            {
                RecurringJob.RemoveIfExists(strJobID);
            }
            catch
            {
                //job has not been run by the scheduler yet or this is the first time running this job, swallow error
            }
        }

        public string GetLastJobID()
        {
            string strJobID = "0";

            using (var connection = JobStorage.Current.GetConnection())
            {
                string lastRunResult = string.Empty;
                var recurringJobs = connection.GetRecurringJobs();
                var job = recurringJobs.FirstOrDefault(p => p.Id.Equals("Viceroy", StringComparison.InvariantCultureIgnoreCase));
                if (job != null)
                {
                    try
                    {
                        RecurringJob.RemoveIfExists(job.Id);
                    }
                    catch
                    {
                        //job has not been run by the scheduler yet or this is the first time running this job, swallow error
                    }
                }
            }
            return strJobID;
        }
        public void InsertIntoConnectionNetworkItemsTable(int ConnID, string strPath, string searchPattern, List<Matter> lstMatters)
        {
            //-----debug

            System.Diagnostics.EventLog log = new System.Diagnostics.EventLog();

            try
            {
                if (!System.Diagnostics.EventLog.SourceExists("InfolinxViceroy"))
                {
                    System.Diagnostics.EventLog.CreateEventSource("InfolinxViceroy", "VroyLog");
                }
            }
            catch { }

            log.Source = "InfolinxViceroy";
            log.WriteEntry("Start of InsertIntoConnectionNetworkItemsTable");
            //----------


            try
            {
                //first clear out rows for this connection
                _ds.ClearConnectionNetworkItemsForConnection(ConnID);

                DataTable objDT = new DataTable();
                objDT.Columns.Add("ID", typeof(int));
                objDT.Columns.Add("CONNECTION_ID", typeof(int));
                objDT.Columns.Add("FOLDER_PATHS", typeof(String));
                objDT.Columns.Add("MATTER_ID", typeof(int));

                List<string> lstPaths = new List<string>();
                int intRowCount = 0;
                int intRunning = 0;

                foreach (Matter matter in lstMatters)
                {
                    lstPaths = new List<string>();

                    string newsearchPattern = searchPattern.Replace("%", matter.ClientFolder);

                    foreach (string clientDir in Directory.GetDirectories(strPath, newsearchPattern))
                    {
                        string matterSearch = searchPattern.Replace("%", matter.MatterFolder);

                        foreach (string path in Directory.GetDirectories(clientDir, matterSearch))
                        {
                            if (path.Length > 0)
                            {
                                lstPaths.Add(path);
                            }
                        }
                    }

                    if (lstPaths.Count > 0)
                    {
                        DataRow newRow = objDT.NewRow();
                        newRow["CONNECTION_ID"] = ConnID.ToString();
                        newRow["FOLDER_PATHS"] = String.Join("€", lstPaths);
                        newRow["MATTER_ID"] = matter.I_ID;

                        try
                        {
                            objDT.Rows.Add(newRow);
                            intRowCount++;
                            intRunning++;
                        }
                        catch (Exception ex)
                        {
                            _logger.LogException("InsertIntoConnectionNetworkItemsTable", ex);                          
                        }
                    }
                }

                objDT.AcceptChanges();

                if (objDT.Rows.Count > 0)
                {
                    try
                    {
                        using (System.Data.SqlClient.SqlBulkCopy bulk = new System.Data.SqlClient.SqlBulkCopy(Properties.Settings.Default.ConnectionString, System.Data.SqlClient.SqlBulkCopyOptions.CheckConstraints))
                        {
                            bulk.DestinationTableName = DatabaseServices.CONNECTION_NETWORK_ITEMS_TABLE;
                            bulk.BulkCopyTimeout = 1000;
                            bulk.BatchSize = 1000;
                            bulk.WriteToServer(objDT);
                        }

                        objDT.Dispose();
                        objDT = null;
                    }
                    catch (Exception ex)
                    {
                        _logger.LogException("InsertIntoConnectionNetworkItemsTable", ex);
                    }
                }

                log.Source = "InfolinxViceroy";
                log.WriteEntry("End of InsertIntoConnectionNetworkItemsTable");
            }
            catch (Exception EX)
            {
                _logger.LogException("InsertIntoConnectionNetworkItemsTable",EX);
            }
        }

        private List<Matter> GetValidMattersList()
        {
            // Get matters from local db
            List<Matter> lstMatters = new List<Matter>();

            DataTable mattersDT = _ds.GetInfolinxItems();
            char[] badChars = Path.GetInvalidPathChars();
            List<string> lstBad = new List<string>();
            lstBad.Add("/");
            foreach (var c in badChars)
            {
                lstBad.Add(c.ToString());
            }

            foreach (DataRow matterRow in mattersDT.Rows)
            {
                bool add = true;
                foreach (string c in lstBad)
                {
                    if (matterRow["FOLDER_NAME"].ToString().Contains(c) || matterRow["PARENT_FOLDER"].ToString().Contains(c))
                    {
                        add = false;
                        break;
                    }
                }
                if (add)
                {
                    Matter m = new Matter(Convert.ToInt32(matterRow["ID"]),
                        Convert.ToInt32(matterRow[DatabaseServices.INFOLINX_ITEMS_COL_I_ID]),
                        matterRow[DatabaseServices.INFOLINX_ITEMS_COL_FOLDER_NAME].ToString(),
                        matterRow[DatabaseServices.INFOLINX_ITEMS_COL_PARENT_FOLDER].ToString());
                    lstMatters.Add(m);
                }
            }

            mattersDT.Clear();
            mattersDT.Dispose();
            mattersDT = null;

            return lstMatters;
        }
        private void InsertFilesforIndexing(int ConnectionID, string strFilter, string IncludeOrExclude, string ArchiveOrDelete, bool IndexContent,DataTable netWorkItemDT)
        {            
            try
            {
                DataTable FolderPathsDT = _ds.GetRowsFromConnectionNetworkItemsForConnection(ConnectionID);

                DataTable dt = new DataTable();
                dt.Columns.Add("ID", typeof(int));
                dt.Columns.Add("I_ID", typeof(int));
                dt.Columns.Add("FOLDER_NAME", typeof(String));
                dt.Columns.Add("FILE_NAME", typeof(String));
                dt.Columns.Add("PATH", typeof(String));
                dt.Columns.Add("RECORD_COUNT", typeof(int));
                dt.Columns.Add("MATTER_ID", typeof(int));
                dt.Columns.Add("UPDATE_INFOLINX", typeof(bool));
                dt.Columns.Add("ON_DELETION", typeof(String));

                string[] filters = strFilter.Split(',');

                HashSet<string> lstUsedPaths = new HashSet<string>();

                foreach (DataRow folderPathRow in FolderPathsDT.Rows)
                {
                    int MatterID = Convert.ToInt32(folderPathRow["MATTER_ID"]);

                    string[] paths = folderPathRow["FOLDER_PATHS"].ToString().Split('€');                   

                    foreach (string path in paths)
                    {
                        List<string> files = new List<string>();

                        if (Directory.Exists(path))
                        {
                            if (IncludeOrExclude == "Exclude")
                            {
                                var allFiles = Directory.GetFiles(path, "*.*");
                                var filesToexclude = filters.SelectMany(f => Directory.GetFiles(path, f).ToArray());
                                files.AddRange(allFiles.Except(filesToexclude));
                            }
                            else //filter is include
                            {
                                files.AddRange(filters.SelectMany(f => Directory.GetFiles(path, f).ToArray()));
                            }

                            if (IndexContent)
                            {
                                foreach (string strPath in files)
                                {
                                    if (netWorkItemDT.Rows.Find(strPath) == null)
                                    {
                                        DirectoryInfo di = new DirectoryInfo(strPath);

                                        string strFileName = Path.GetFileName(strPath);

                                        DataRow row = dt.NewRow();
                                        row["FOLDER_NAME"] = path;
                                        row["FILE_NAME"] = strFileName;
                                        row["PATH"] = strPath;
                                        row["RECORD_COUNT"] = 1;
                                        row["MATTER_ID"] = MatterID;
                                        row["UPDATE_INFOLINX"] = false;
                                        row["ON_DELETION"] = ArchiveOrDelete;

                                        if (!lstUsedPaths.Contains(strPath))
                                        {
                                            dt.Rows.Add(row);
                                            lstUsedPaths.Add(strPath);

                                            if (DatabaseServices.VerboseLogging && lstUsedPaths.Count % 10000 == 0)
                                            {
                                                _logger.LogMessageToFile(lstUsedPaths.Count.ToString() + " files added to datatable in InsertFilesforIndexing");
                                            }

                                        }
                                        else
                                        {
                                            _logger.LogMessageToFile("Duplicate path " + strPath + " Matter ID =" + MatterID.ToString());
                                        }
                                    }                                  
                                }
                            }
                            else
                            {

                                object[] PathCountKeys = new object[1] {path};

                                bool updateRow = false;

                                DataRow existingRow = netWorkItemDT.Rows.Find(PathCountKeys);

                                if (existingRow != null && files.Count != Convert.ToInt32(existingRow["RECORD_COUNT"]))
                                {
                                    updateRow = true;
                                    //just update network items table then
                                    _ds.UpdateRecordCountInNetworkTable(Convert.ToInt32(existingRow["ID"]), files.Count);
                                    continue;
                                }

                                DataRow row = dt.NewRow();
                                row["FOLDER_NAME"] = path;
                                row["FILE_NAME"] = DBNull.Value;
                                row["PATH"] = path;
                                row["RECORD_COUNT"] = files.Count;
                                row["MATTER_ID"] = MatterID;
                                row["UPDATE_INFOLINX"] = updateRow;
                                row["ON_DELETION"] = ArchiveOrDelete;                                

                                if (existingRow == null)
                                {
                                    dt.Rows.Add(row);
                                  
                                    if (DatabaseServices.VerboseLogging && lstUsedPaths.Count % 10000 == 0)
                                    {
                                        _logger.LogMessageToFile(files.Count.ToString() + " records added to datatable in InsertFilesforIndexing-Count");
                                    }

                                }                               
                            }
                        }
                    }
                }

                dt.AcceptChanges();

                if (dt.Rows.Count > 0)
                {
                    if (DatabaseServices.VerboseLogging)
                    {
                        _logger.LogMessageToFile(dt.Rows.Count.ToString() + " about to be bulk inserted to datatable into NETWORK_ITEMS table");
                    }

                    try
                    {
                        using (System.Data.SqlClient.SqlBulkCopy bulk = new System.Data.SqlClient.SqlBulkCopy(Properties.Settings.Default.ConnectionString, System.Data.SqlClient.SqlBulkCopyOptions.CheckConstraints))
                        {
                            bulk.DestinationTableName = "NETWORK_ITEMS";
                            bulk.BulkCopyTimeout = 1000;
                            bulk.BatchSize = 1000;
                            bulk.WriteToServer(dt);
                        }

                        return;
                    }
                    catch (Exception ex)
                    {                       
                        _logger.LogException("NETWORK_ITEMS", ex);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogException("InsertFilesforIndexing", ex);               
            }

        }

    }
    public class Matter
    {
        public int ID { get; set; }
        public int I_ID { get; set; }

        /// <summary>
        /// FOLDER_NAME
        /// </summary>
        public string MatterFolder { get; set; }
        /// <summary>
        /// PARENT_FOLDER column
        /// </summary>
        public string ClientFolder { get; set; }
        public string PhysicalPath { get; set; }

        public Matter(int id, int i_id, string foldername, string parentfolder)
        {
            this.ID = id;
            this.I_ID = i_id;
            this.MatterFolder = foldername;
            this.ClientFolder = parentfolder;
        }

        public Matter(int id, int i_id, string foldername, string parentfolder, string physicalpath)
        {
            this.ID = id;
            this.I_ID = i_id;
            this.MatterFolder = foldername;
            this.ClientFolder = parentfolder;
            this.PhysicalPath = physicalpath;
        }
    }


}
