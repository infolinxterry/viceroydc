﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace ViceroyService
{
    public class StringDecrypter
    {
        private static byte[] objKeyByteArray =  {
            13,253,5,50,52,91,193,133,193,121,221,29,57,128,91,91,19,13,13,197,125,98,89,48,97,154,83,187,222,167,171,13
        };

        private static byte[] ivb = {
            10,61,235,20,122,120,80,248,13,182,196,212,176,46,13,85
        };

        public static string EncryptString(string src)
        {
            byte[] p = Encoding.ASCII.GetBytes(src.ToCharArray());
            byte[] encodedBytes = { };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged rv = new RijndaelManaged())
                {
                    using (CryptoStream cs = new CryptoStream(ms, rv.CreateEncryptor(objKeyByteArray, ivb), CryptoStreamMode.Write))
                    {
                        cs.Write(p, 0, p.Length);
                        cs.FlushFinalBlock();
                        encodedBytes = ms.ToArray();
                    }
                }
            }
            return Convert.ToBase64String(encodedBytes);
        }

        public static string DecryptString(string src)
        {

            // to be a base 64 string the 'src' param must be a
            //	multiple of 4
            if (src == null || src.Length < 4 || src.Length % 4 != 0)
            {
                // either the string to decrypt was null, or it was less than 4,
                //	or the number of characters was not a multiple of 4
                return null;
            }

            byte[] p = Convert.FromBase64String(src);
            byte[] initialText = new Byte[p.Length];

            using (RijndaelManaged objRijndael = new RijndaelManaged())
            {
                using (MemoryStream ms = new MemoryStream(p))
                {
                    using (CryptoStream cs = new CryptoStream(ms, objRijndael.CreateDecryptor(objKeyByteArray, ivb), CryptoStreamMode.Read))
                    {
                        cs.Read(initialText, 0, initialText.Length);
                    }
                }
            }

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < initialText.Length; ++i)
            {
                char c = (char)initialText[i];
                if (c != '\0')
                {
                    sb.Append(c);
                }
            }

            return sb.ToString();
        }
    }
}
