﻿using Hangfire;
using Hangfire.SqlServer;
using System;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Xml;

namespace ViceroyService
{
    public class Main
    {
        private BackgroundJobServer _server;

        protected void DoWork()
        {
            string connectionString;

            try
            {
                connectionString = GetConnectionStringFromConfigFile();
                GlobalConfiguration.Configuration.UseSqlServerStorage(connectionString);
                _server = new BackgroundJobServer();
            }
            catch (Exception ex)
            {
                EventLog eventLog = new EventLog();
                eventLog.Source = "Viceroy";
                eventLog.WriteEntry(string.Format("Error starting service. {0}Exception Message: {1}{2}Stack Trace: {3}",
                    Environment.NewLine, ex.Message, Environment.NewLine, ex.StackTrace));

                Stop();
                return;
            }

            DatabaseServices ds = new DatabaseServices(connectionString);          
            
            //get all properties set in controller
            DataTable dt = null;

            try
            {
                dt = ds.GetVRSettings();
            }
            catch (Exception ex)
            {
                EventLog eventLog = new EventLog();
                eventLog.Source = "Viceroy";
                eventLog.WriteEntry("Error connecting to database " + connectionString + " "  + ex.ToString());                
            }

            ds.UpdateHangFireJobTable();

            foreach (DataRow row in dt.Rows)
            {
                switch (row["SETTING_NAME"].ToString())
                {
                    case "DeleteLogFileAfterDays":
                        DatabaseServices.DeleteLogFileAfterDays = row["SETTING_VALUE"] != DBNull.Value ? Convert.ToInt32(row["SETTING_VALUE"]) : 5;
                        break;
                    case "Password":
                        DatabaseServices.Password = row["SETTING_VALUE"] != DBNull.Value ? row["SETTING_VALUE"].ToString() : "";
                        break;
                    case "Frequency":
                        DatabaseServices.Frequency = row["SETTING_VALUE"] != DBNull.Value ? Convert.ToInt32(row["SETTING_VALUE"]) : 5;
                        break;
                    case "MattersTab":
                        DatabaseServices.MattersTab = row["SETTING_VALUE"] != DBNull.Value ? Convert.ToInt32(row["SETTING_VALUE"]) : 218;
                        break;
                    case "InfolinxColumnNames":
                        DatabaseServices.InfolinxColumnNames = row["SETTING_VALUE"] != DBNull.Value ? row["SETTING_VALUE"].ToString() : "";
                        break;
                    case "InfolinxRestBaseAddress":
                        DatabaseServices.InfolinxRestBaseAddress = row["SETTING_VALUE"] != DBNull.Value ? row["SETTING_VALUE"].ToString() : "https://akingump.infolinx.com:81";
                        break;
                    case "UserName":
                        DatabaseServices.UserName = row["SETTING_VALUE"] != DBNull.Value ? row["SETTING_VALUE"].ToString() : "SuperUser";
                        break;
                    case "MatterCode":
                        DatabaseServices.MatterCode = row["SETTING_VALUE"] != DBNull.Value ? row["SETTING_VALUE"].ToString() : "MATTER_CODE";
                        break;
                    case "ItemTab":
                        DatabaseServices.ItemTab = row["SETTING_VALUE"] != DBNull.Value ? Convert.ToInt32(row["SETTING_VALUE"]) : 218;
                        break;
                    case "ArchiveLocation":
                        DatabaseServices.ArchiveLocation = row["SETTING_VALUE"] != DBNull.Value ? row["SETTING_VALUE"].ToString() : Environment.CurrentDirectory;
                        break;
                    case "LogFilePath":
                        DatabaseServices.LogFilePath = row["SETTING_VALUE"] != DBNull.Value ? row["SETTING_VALUE"].ToString() : Path.Combine(Environment.CurrentDirectory, "log"); 
                        break;
                    case "DirectoryRefreshTime":
                        DatabaseServices.DirectoryRefreshTime = row["SETTING_VALUE"] != DBNull.Value ? Convert.ToInt32(row["SETTING_VALUE"]) : 5;
                        break;
                    case "VerboseLogging":
                        DatabaseServices.VerboseLogging = row["SETTING_VALUE"] != DBNull.Value ? Convert.ToBoolean(row["SETTING_VALUE"]) : true;
                        break;
                }
                
            }

            Logger logger = new Logger();
            InfolinxItem item = new InfolinxItem();

            FileShareAgent fsa = new FileShareAgent();
            var JobID = BackgroundJob.Schedule(() => fsa.SynchronizeContent(), TimeSpan.Zero);           

            
        }

        public string GetConnectionStringFromConfigFile()
        {
            string connectionString = null;
            string configFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ViceroyService.exe.config");

            try
            {
                XmlDocument configFile = new XmlDocument();
                configFile.Load(configFilePath);
                XmlNode connectionStringNode = configFile.SelectSingleNode("//setting[@name='ConnectionString']");
                connectionString = connectionStringNode.InnerText;
            }
            catch (Exception ex)
            {
                EventLog eventLog = new EventLog();
                eventLog.Source = "Viceroy";
                eventLog.WriteEntry(string.Format("Error reading connection string from configuration file. {0}Exception Message: {1}{2}Stack Trace: {3}",
                    Environment.NewLine, ex.Message, Environment.NewLine, ex.StackTrace));

                throw;
            }

            return connectionString;
        }

        public void Start()
        {
            DoWork();
        }

        public void Stop()
        {
            if (_server != null)
            {
                _server.Dispose();
            }
        }
    }
}
