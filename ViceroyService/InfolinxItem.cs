﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data;

namespace ViceroyService
{
    public class IxItem
    {
        public string Path { get; set; }
        public int Count { get; set; }
        public int ExternalID { get; set; }        
        public int ExternalType { get; set; }
        public string FolderName { get; set; }
        public string FileName { get; set; }
        public int MatterID { get; set; }
        public int ItemID { get; set; }

        public IxItem(int intItemID,int intCount,int intExternalID)
        {
            this.ItemID = intItemID;
            this.Count = intCount;
            this.ExternalID = intExternalID;
        }

        public IxItem(string strPath, int intCount, int intExternalID, int intExternalType, string strFolderName, string strFileName, int intMatterID)
        {
            this.Path = strPath;
            this.Count = intCount;
            this.ExternalID = intExternalID;
            this.ExternalType = intExternalType;
            this.FolderName = strFolderName;
            this.FileName = strFileName;
            this.MatterID = intMatterID;
        }
    }
    public class InfolinxItem
    {
        private const string ITEM_COL_I_DELETED     = "I_DELETED";
        private const string ITEM_COL_I_MODIFY_DATE = "I_MODIFY_DATE";
        private const string ITEM_COL_I_ID          = "I_ID";
        private const string ITEM_COL_I_IT_ID       = "I_IT_ID";

        private Logger _logger;

        public InfolinxItem()
        {
            _logger = new Logger();
        }

        public async static Task<bool> EstablishConnectionWithWebService(string UserName, string Password, string Url)
        {
            bool blnRet = false;

            DatabaseServices.UserName = UserName;
            DatabaseServices.Password = StringDecrypter.EncryptString(Password);
            DatabaseServices.InfolinxRestBaseAddress = Url;
           // Properties.Settings.Default.Save();          

            IRTab itab = await InfolinxRestProxy.GetTab(1);

            if (itab.Id == 1)
            {
                blnRet = true;
            }

            return blnRet;
        }

        /// <summary>
        /// get all the matters greater than the max i_id we have in the INFOLINX_ITEMS table and insert them into the INFOLINX_ITEMS table
        /// </summary>
        public async Task GetInfolinxMatters()
        {
            DatabaseServices DS = new DatabaseServices(Properties.Settings.Default.ConnectionString);

            int itemID = DS.GetMaxInfolinxItemId();

            try
            {
                List<IRSimpleItem> allMatters = await GetAllMatters(itemID);

                // Insert these matters into DB
                foreach (var matter in allMatters)
                {
                    DS.InsertIntoInfolinxItemsTable(matter.I, matter.B, matter.D, matter.T, matter.G, Convert.ToInt32(matter.C), matter.R);
                }               
            }
            catch (Exception ex)
            {
                _logger.LogException("GetInfolinxMatters", ex);               
            }

            _logger.LogMessageToFile("Done getting matters");
        }

        /// <summary>
        /// gets matters 1000 at a time where the i_id is greater than the one passed in
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns></returns>
        public async Task<List<IRSimpleItem>> GetAllMatters(int itemID)
        {
            List<IRSimpleItem> allMatters = new List<IRSimpleItem>();
            List<IRSimpleItem> batch = new List<IRSimpleItem>();

            do
            {
                batch = await InfolinxRestProxy.GetMatters(itemID, DatabaseServices.MattersTab, DatabaseServices.MatterCode,
                    DatabaseServices.ClientCode, DatabaseServices.ClientID) as List<IRSimpleItem>;
                allMatters.AddRange(batch);
                if (batch.Count > 0)
                {
                    itemID = batch[batch.Count - 1].I;
                }
            }
            while (batch.Count == 1000);

            return allMatters;
        }

        /// <summary>
        /// we need to get all items that are deleted
        /// </summary>
        public async Task<List<IRSimpleItem>> GetDeletedItemsFromInfolinx()
        {
            List<IRSimpleItem> allExternalItems = new List<IRSimpleItem>();
            List<IRSimpleItem> batch = new List<IRSimpleItem>();

            int itemID = -1;

            Dictionary<string, object> objSearch = new Dictionary<string, object>();

            DatabaseServices ds = new DatabaseServices(Properties.Settings.Default.ConnectionString);

            objSearch.Add(ITEM_COL_I_DELETED, 1);
            DateTime ? objDt = ds.GetMaxModifyDate();
            if (objDt != null)
            {
                objSearch.Add(ITEM_COL_I_MODIFY_DATE, objDt);
            }
            objSearch.Add(ITEM_COL_I_ID, -1);
            objSearch.Add("FK_EXTERNAL_TYPE_ID", "en-US_" + DatabaseServices.ExternalTypeFileShare.ToString());//make sure we only get items in this tab from of type FileShare

            do
            {
                batch = await InfolinxRestProxy.GetExternalItems(DatabaseServices.ItemTab, objSearch) as List<IRSimpleItem>;

                objSearch.Remove(ITEM_COL_I_ID);

                allExternalItems.AddRange(batch);
                if (batch.Count > 0)
                {
                    itemID = batch[batch.Count - 1].I;
                    objSearch.Add(ITEM_COL_I_ID, itemID);
                }
            }
            while (batch.Count == 1000);

            return allExternalItems;
        }

        public async Task DeleteItemInInfolinx(int ItemID)
        {
            DatabaseServices ds = new DatabaseServices(Properties.Settings.Default.ConnectionString);

            try
            {
                string[] strColumnNames = DatabaseServices.InfolinxColumnNames.Split(",".ToCharArray());
                IRColumn clmiid = new IRColumn();
                clmiid.ColumnName = ITEM_COL_I_ID;
                clmiid.Value = ItemID;              

                IRColumn tabId = new IRColumn();
                tabId.ColumnName = ITEM_COL_I_IT_ID;
                tabId.Value = DatabaseServices.ItemTab; //this is the item we are updating - need this to get the item type data row in web service

                IRItem item = new IRItem();

                item.columns = new List<IRColumn> { };

                item.columns.Add(tabId);
                item.columns.Add(clmiid);
                
                List<IRItem> iList = new List<IRItem>();

                iList.Add(item);

                IRItemResult i = await InfolinxRestProxy.DeleteItem(ItemID);

                //now delete from local table
                if (i.Result == true)
                {
                    ds.DeleteNetworkItemByInfolinxId(ItemID);
                }
               
            }
            catch (Exception ex)
            {
                string s = ex.ToString();
            }
        }

        public async Task UpdateItemsInInfolinx(List<IxItem> lstItems)
        {
            DatabaseServices ds = new DatabaseServices(Properties.Settings.Default.ConnectionString);

            string[] strColumnNames = DatabaseServices.InfolinxColumnNames.Split(",".ToCharArray());

            List<IRItem> iList = new List<IRItem>();

            try
            {
                foreach (IxItem iItem in lstItems)
                {
                    IRColumn clmiid = new IRColumn();
                    clmiid.ColumnName = ITEM_COL_I_ID;
                    clmiid.Value = iItem.ItemID;

                    IRColumn clmCount = new IRColumn();
                    clmCount.ColumnName = strColumnNames[3]; //count #
                    clmCount.Value = iItem.Count;

                    IRColumn tabId = new IRColumn();
                    tabId.ColumnName = ITEM_COL_I_IT_ID;
                    tabId.Value = DatabaseServices.ItemTab; //this is the item we are updating - need this to get the item type data row in web service

                    //always put in the external id column
                    IRColumn clmExternalID = new IRColumn();
                    clmExternalID.ColumnName = "EXTERNAL_ID"; //file name
                    clmExternalID.Value = iItem.ExternalID;

                    IRItem item = new IRItem();

                    item.columns = new List<IRColumn> { };

                    item.columns.Add(tabId);
                    item.columns.Add(clmiid);
                    item.columns.Add(clmCount);
                    item.columns.Add(clmExternalID);

                    iList.Add(item);

                    if (iList.Count == 500)
                    {
                        IRItemResultList i = await InfolinxRestProxy.CreateItem(iList);

                        iList.Clear();

                        foreach (IRItemResult iresult in i.ItemResults)
                        {
                            ds.RemoveUpdateFlagOnInfolinxItem(iresult.ItemId);
                        }
                    }
                }
                if (iList.Count > 0)
                {
                    IRItemResultList i = await InfolinxRestProxy.CreateItem(iList);

                    iList.Clear();

                    foreach (IRItemResult iresult in i.ItemResults)
                    {
                        ds.RemoveUpdateFlagOnInfolinxItem(iresult.ItemId);
                    }
                }
            }
            catch (Exception ex)
            {
                string s = ex.ToString();
            }
            
        }

        public async Task UpdateItemInInfolinx(int ItemID, int count)
        {
            DatabaseServices ds = new DatabaseServices(Properties.Settings.Default.ConnectionString);

            try
            {
                string[] strColumnNames = DatabaseServices.InfolinxColumnNames.Split(",".ToCharArray());
                IRColumn clmiid = new IRColumn();
                clmiid.ColumnName = ITEM_COL_I_ID;
                clmiid.Value = ItemID;             

                IRColumn clmCount = new IRColumn();
                clmCount.ColumnName = strColumnNames[3]; //count #
                clmCount.Value = count;

                IRColumn tabId = new IRColumn();
                tabId.ColumnName = ITEM_COL_I_IT_ID;
                tabId.Value = DatabaseServices.ItemTab; //this is the item we are updating - need this to get the item type data row in web service

                IRItem item = new IRItem();

                item.columns = new List<IRColumn> { };

                item.columns.Add(tabId);
                item.columns.Add(clmiid);                                
                item.columns.Add(clmCount);               

                List<IRItem> iList = new List<IRItem>();

                iList.Add(item);
                
                IRItemResultList i = await InfolinxRestProxy.CreateItem(iList);

                foreach (IRItemResult iresult in i.ItemResults)
                {
                    ds.RemoveUpdateFlagOnInfolinxItem(ItemID);
                }
            }
            catch(Exception ex)
            {
                string s = ex.ToString();
            }

        }
        public async Task CreateItemsInInfolinx(List<IxItem> lstItems)
        {
            DatabaseServices ds = new DatabaseServices(Properties.Settings.Default.ConnectionString);

            string[] strColumnNames = DatabaseServices.InfolinxColumnNames.Split(",".ToCharArray());

            DataTable objDT = new DataTable();
            objDT.Columns.Add("ID", typeof(int));
            objDT.Columns.Add("I_ID", typeof(int));

            try
            {
                List<IRItem> iList = new List<IRItem>();

                foreach (IxItem iItem in lstItems)
                {                   
                    IRColumn clmFileName = new IRColumn();
                    clmFileName.ColumnName = strColumnNames[0]; //file name
                    clmFileName.Value = iItem.FileName == String.Empty ? iItem.FolderName : iItem.FileName;
                   

                    IRColumn clmNetWorkPath = new IRColumn();
                    clmNetWorkPath.ColumnName = strColumnNames[1]; //network path
                    clmNetWorkPath.Value = iItem.Path;

                    IRColumn clmMatter = new IRColumn();
                    clmMatter.ColumnName = strColumnNames[2]; //matter # - if this is a drop down like it will be for AG we can use the rows in INFOLINX_ITEMS to match text and get the id
                    clmMatter.Value = iItem.MatterID;

                    IRColumn clmCount = new IRColumn();
                    clmCount.ColumnName = strColumnNames[3]; //count #
                    clmCount.Value = iItem.Count;

                    IRColumn clmExternaleType = new IRColumn();
                    clmExternaleType.ColumnName = strColumnNames[4]; //exteranl type # Access, File Share - ...
                    clmExternaleType.Value =  "en-US_" + iItem.ExternalType.ToString();

                    IRColumn tabId = new IRColumn();
                    tabId.ColumnName = ITEM_COL_I_IT_ID;
                    tabId.Value = DatabaseServices.ItemTab; //this is the item we are creating

                    //always put in the external id column
                    IRColumn clmExternalID = new IRColumn();
                    clmExternalID.ColumnName = "EXTERNAL_ID";
                    clmExternalID.Value = iItem.ExternalID;

                    IRItem item = new IRItem();

                    item.columns = new List<IRColumn> { };

                    item.columns.Add(tabId);

                    item.columns.Add(clmFileName);
                    item.columns.Add(clmNetWorkPath);
                    item.columns.Add(clmMatter);
                    item.columns.Add(clmCount);
                    item.columns.Add(clmExternaleType);
                    item.columns.Add(clmExternalID);

                    iList.Add(item);

                    if (iList.Count == 500)
                    {
                        IRItemResultList i = await InfolinxRestProxy.CreateItem(iList);

                        foreach (IRItemResult iresult in i.ItemResults)
                        {
                            if(iresult.RequestId != -1)
                            {
                                DataRow dr = objDT.NewRow();
                                dr["ID"] = iresult.RequestId;
                                dr["I_ID"] = iresult.ItemId;
                                objDT.Rows.Add(dr);
                            }                           
                        }

                        iList.Clear();
                    }                       
                }

                if (iList.Count > 0)
                {
                    IRItemResultList i = await InfolinxRestProxy.CreateItem(iList);

                    foreach (IRItemResult iresult in i.ItemResults)
                    {
                        if (iresult.RequestId != -1)
                        {
                            DataRow dr = objDT.NewRow();
                            dr["ID"] = iresult.RequestId;
                            dr["I_ID"] = iresult.ItemId;
                            objDT.Rows.Add(dr);
                        }                        
                    }

                    iList.Clear();
                }

                if (DatabaseServices.VerboseLogging)
                {
                    _logger.LogMessageToFile("about to call BulkCopyItemIDs with " + objDT.Rows.Count.ToString() + " items");
                }

                ds.BulkCopyItemIDs(objDT); //Put all of the ids into a table

                if (DatabaseServices.VerboseLogging)
                {
                    _logger.LogMessageToFile("about to call UpdateNetworkItemIDs for " + objDT.Rows.Count.ToString() + " items");
                }

                ds.UpdateNetworkItemIDs(); //called stored proc that will update NETWORK_ITEMS table and then delete all items in ITEM_IDS table          
            }
            catch (Exception ex)
            {
                string s = ex.ToString();
            }

        }

        public async Task CreateItemInInfolinx(string strPath, int count,int LocalID,int ExternalType,string strFolderName,string strFileName,int MatterID)
        {                       
            DatabaseServices ds = new DatabaseServices(Properties.Settings.Default.ConnectionString);

            string strExternalSource = "en-US_" + ExternalType.ToString();

            //Matter is  a list
            // int MatterID = ds.GetItemIdForFolderName(strFolderName);

            try
            {

                string[] strColumnNames = DatabaseServices.InfolinxColumnNames.Split(",".ToCharArray());

                IRColumn clmFileName = new IRColumn();
                clmFileName.ColumnName = strColumnNames[0]; //file name
                clmFileName.Value = strFileName;

                IRColumn clmNetWorkPath = new IRColumn();
                clmNetWorkPath.ColumnName = strColumnNames[1]; //network path
                clmNetWorkPath.Value = strPath;

                IRColumn clmMatter = new IRColumn();
                clmMatter.ColumnName = strColumnNames[2]; //matter # - if this is a drop down like it will be for AG we can use the rows in INFOLINX_ITEMS to match text and get the id
                clmMatter.Value = MatterID;

                IRColumn clmCount = new IRColumn();
                clmCount.ColumnName = strColumnNames[3]; //count #
                clmCount.Value = count;

                IRColumn clmExternaleType = new IRColumn();
                clmExternaleType.ColumnName = strColumnNames[4]; //exteranl type # Access, File Share - ...
                clmExternaleType.Value = strExternalSource;


                //IRColumn clmClientID = new IRColumn();
                //clmClientID.ColumnName = Properties.Settings.Default.ClientID;
                //clmClientID.Value = ClientID;



                IRColumn tabId = new IRColumn();
                tabId.ColumnName = ITEM_COL_I_IT_ID;
                tabId.Value = DatabaseServices.ItemTab; //this is the item we are creating

                IRItem item = new IRItem();

                item.columns = new List<IRColumn> { };

                item.columns.Add(tabId);

                item.columns.Add(clmFileName);
                item.columns.Add(clmNetWorkPath);
                item.columns.Add(clmMatter);
                item.columns.Add(clmCount);
                item.columns.Add(clmExternaleType);
               // item.columns.Add(clmClientID);

                List<IRItem> iList = new List<IRItem>();

                iList.Add(item);

                IRItemResultList i = await InfolinxRestProxy.CreateItem(iList);

                foreach (IRItemResult iresult in i.ItemResults)
                {
                    ds.UpdateItemsNetworkTable(LocalID, iresult.ItemId);
                }           
                
                if(i.ItemResults.Count == 0)
                {
                    _logger.LogMessageToFile("Error creating item in Infolinx - " + i.Message);
                }
            }
            catch(Exception ex)
            {
                string s = ex.ToString();
                _logger.LogException("CreateItemInInfolinx", ex);
            }
        }
    }
}
