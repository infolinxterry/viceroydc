﻿using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ViceroyService
{
    public class Synchronizer
    {
        private DatabaseServices _databaseServices;

        public Synchronizer()
        {
            _databaseServices = new DatabaseServices(Properties.Settings.Default.ConnectionString);
        }

        public async Task<string[]> GetAllFiles(string filters, string includeOrExclude, bool includeSubdirectories)
        {
            HashSet<string> files = new HashSet<string>();

            Dictionary<int, string> matterDirectoryMap = await MatchInfolinxMattersToMatterDirectories("%");
            string filterRegex = ConvertFilterArrayToSingleRegex(filters.Split(','));

            foreach (var matter in matterDirectoryMap)
            {
                string[] allFiles = Directory.GetFiles(matter.Value, "*",
                    includeSubdirectories ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);

                if (includeOrExclude.ToLower() == "include")
                {
                    files.UnionWith(allFiles.Where(f => Regex.IsMatch(new FileInfo(f).Name, filterRegex)));
                }
                else if (includeOrExclude.ToLower() == "exclude")
                {
                    files.UnionWith(allFiles.Where(f => !Regex.IsMatch(new FileInfo(f).Name, filterRegex)));
                }
            }

            return files.ToArray();
        }

        public async Task<Dictionary<int, string>> MatchInfolinxMattersToMatterDirectories(string searchPattern)
        {
            Dictionary<int, string> matterDirectoryMap = new Dictionary<int, string>();

            InfolinxItem infolinx = new InfolinxItem();
            List<IRSimpleItem> matters = await infolinx.GetAllMatters(-1);
            matters = matters.Where(matter => !matter.G.EndsWith(")")).ToList();
            string[] matterDirectories = GetAllMatterDirectories();

            foreach (var matter in matters)
            {
                // TODO: Add code to validate matter codes
                IEnumerable<string> matches = matterDirectories
                    .Where(d => Regex.IsMatch(d, searchPattern.Replace("%", matter.G).Replace("*", @"[\w\-. ]+")));

                if (matches.Any())
                {
                    matterDirectoryMap.Add(matter.I, matches.First());
                }
                else
                {
                    // Log some message about no directories being found for this matter
                }
            }

            return matterDirectoryMap;
        }

        public string[] GetAllMatterDirectories()
        {
            HashSet<string> matterDirectories = new HashSet<string>();

            //string[] clientDirectories = GetClientDirectoriesForAllConnections();
            string[] clientDirectories = GetClientsForTest();

            foreach (var clientDirectory in clientDirectories)
            {
                if (Directory.Exists(clientDirectory))
                {
                    matterDirectories.UnionWith(Directory.GetDirectories(clientDirectory));
                }
                else
                {
                    // Log some message about the client directory not existing
                }
            }

            return matterDirectories.ToArray();
        }

        public string[] GetClientsForTest()
        {
            return Directory.GetDirectories(@"C:\AirTravel\ok");
        }

        public string[] GetClientDirectoriesForAllConnections()
        {
            HashSet<string> clientDirectories = new HashSet<string>();

            DataTable connections = _databaseServices.GetConnections();

            foreach (DataRow connection in connections.Rows)
            {
                string baseDirectoryPath = connection["PATH"].ToString();

                if (Directory.Exists(baseDirectoryPath))
                {
                    clientDirectories.UnionWith(Directory.GetDirectories(baseDirectoryPath));
                }
                else
                {
                    // Log some message about the base directory not existing for this connection
                }
            }

            return clientDirectories.ToArray();
        }

        private string ConvertFilterArrayToSingleRegex(string[] filters)
        {
            string regex = "^" + string.Join("|", filters).Replace("*", @"[\w\-. ]+") + "$";

            if (filters.Length > 1)
            {
                regex.Insert(1, "(").Insert(filters.Length - 1, ")");
            }

            return regex;
        }
    }
}