﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;


namespace ViceroyService
{
    #region Models
    public partial class IRItemResultList
    {
        public string Message { get; set; }
        public bool Result { get; set; }
        public List<IRItemResult> ItemResults { get; set; }
    }
    public partial class IRItemSearch
    {
        public virtual String SpecialSearch { get; set; }
        public virtual DateTime LastSyncDate { get; set; }
        public virtual Int32 TabId { get; set; }
        public virtual String Barcode { get; set; }
        public virtual Dictionary<String, Object> SearchTerms { get; set; }
        public virtual Boolean IncludeEdocFile { get; set; }
        public virtual Boolean ViewDisplay { get; set; }
        public virtual Boolean GetForeignKeyDesc { get; set; }
    }
    public partial class IRItemResult
    {
        public virtual String LocationBarcode { get; set; }
        public virtual String LocationDescription { get; set; }
        public virtual String ItemBarcode { get; set; }
        public virtual String ItemDescription { get; set; }
        public virtual Boolean Result { get; set; }
        public virtual Int32 Reason { get; set; }
        public virtual String Message { get; set; }
        public virtual int ItemId { get; set; }
        public virtual int RequestId { get; set; }
    }
    #endregion
    #region Models
    public partial class IRItem
    {
        public virtual List<IRColumn> columns { get; set; }
        public virtual String TabSingularName { get; set; }
        public virtual String EdocFile { get; set; }
        public virtual String ContentType { get; set; }
        public virtual Int32 Id { get; set; }
        public virtual String QuickDescription { get; set; }
        public virtual String Barcode { get; set; }
        public virtual Int32 TabId { get; set; }
    }

    public partial class IRSimpleItem
    {
        public string B { get; set; }
        public string D { get; set; }
        public int T { get; set; }
        public string R { get; set; }
        /// <summary>
        /// generic column name
        /// </summary>
        public string G { get; set; }

        public int I { get; set; }

        public string C { get; set; }
    }
    #endregion
    #region Models
    public partial class IRRequestSearch
    {
        public virtual Boolean OnlyMine { get; set; }
        public virtual Boolean GetDescriptions { get; set; }
    }
    #endregion
    #region Models
    public partial class IRRequest
    {
        public virtual Int32 Id { get; set; }
        public virtual Int32 ItemId { get; set; }
        public virtual String ItemDescription { get; set; }
        public virtual Int32 DestinationItemId { get; set; }
        public virtual String DestinationDescription { get; set; }
        public virtual Boolean Waitlist { get; set; }
        public virtual DateTime CreateDate { get; set; }
        public virtual String RequestorName { get; set; }
        public virtual String RequestorDesc { get; set; }
        public virtual Int32 Status { get; set; }
        public virtual Int32 BatchNumber { get; set; }
        public virtual Int32 FulfillmentMethod { get; set; }
        public virtual Int32 RequestType { get; set; }
        public virtual String Comment { get; set; }
    }
    #endregion
    #region Models
    public partial class IRLabel
    {
        public virtual Int32 QueueId { get; set; }
        public virtual Int32 ItemId { get; set; }
        public virtual String ItemBarcode { get; set; }
        public virtual String Reason { get; set; }
    }
    #endregion
    #region Models
    public partial class IRInterchange
    {
    }
    #endregion
    #region Models

    public partial class IRTransfer
    {
        public virtual String Location { get; set; }
        public virtual List<String> Items { get; set; }
    }
    #endregion
    #region Models
    public partial class IRDictionarySearch
    {
        public virtual Int32 TabId { get; set; }
        public virtual String SpecialSearch { get; set; }
    }
    #endregion
    #region Models
    public partial class IRColumn
    {
        public virtual String ColumnName { get; set; }
        public virtual String Caption { get; set; }
        public virtual Object Value { get; set; }
    }
    public partial class IRTab
    {
        public virtual Int32 Id { get; set; }
        public virtual String SingularName { get; set; }
        public virtual String PluralName { get; set; }
        public virtual Boolean IsMoveable { get; set; }
        public virtual Boolean IsRequestable { get; set; }
        public virtual Boolean AutoGenerateBarcode { get; set; }
        public virtual Boolean IsBarcodeRequired { get; set; }
        public virtual String BarcodePrefix { get; set; }
        public virtual Int32 BarcodeLength { get; set; }
        public virtual Boolean IsEdoc { get; set; }
        public virtual Int32 DisplayOrder { get; set; }
        public virtual Boolean IsRetentionEnabled { get; set; }
        public virtual Int32 SpecialType { get; set; }
    }
    public partial class IRPicklistItem
    {
        public virtual Dictionary<string, object> Columns { get; set; }
        public virtual string QuickDescription { get; set; }
        public virtual int Id { get; set; }
        public int TabId { get; set; }
    }
    #endregion

    //-------

    public class IRIO
    {
        public string Results { get; set; }
        public string FileName { get; set; }
        public string Extension { get; set; }
        public int Position { get; set; }
        public int CountOfBytesReceived { get; set; }
        public long TotalFileLength { get; set; }
        public FileLocation FileLocationType { get; set; }
        public enum FileLocation
        {
            Edoc = 1,
            Interchange = 2,
            Label = 3,
            Report = 4
        }
        public long MaxFileTransferSize { get; set; }
        public string FilePath { get; set; }
        public bool Complete { get; set; }
        public byte[] buffer { get; set; }

    }
    //---------
    public class IRDictionary
    {
        public string Caption { get; set; }
        public string ColumnName { get; set; }
    }


}


namespace ViceroyService
{
    public partial class ItemClient : IDisposable
    {

        public HttpClient HttpClient { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public ItemClient()
        {
            HttpClient = new HttpClient()
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public ItemClient(HttpMessageHandler handler, bool disposeHandler = true)
        {
            HttpClient = new HttpClient(handler, disposeHandler)
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual async Task<HttpResponseMessage> PostAsync(IRItemSearch value)
        {
            return await HttpClient.PostAsJsonAsync<IRItemSearch>("api/itemsearch", value).ConfigureAwait(false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual async Task<HttpResponseMessage> GetAsync(Int32 id)
        {
            return await HttpClient.GetAsync("api/Item/" + id).ConfigureAwait(false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual async Task<HttpResponseMessage> PostAsync(IEnumerable<IRItem> value)
        {
            return await HttpClient.PostAsJsonAsync<IEnumerable<IRItem>>("api/Item", value).ConfigureAwait(false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual async Task<HttpResponseMessage> DeleteAsync(Int32 id)
        {
            return await HttpClient.DeleteAsync("api/Item/" + id).ConfigureAwait(false);
        }


       

        #endregion

        public void Dispose()
        {
            HttpClient.Dispose();
        }
    }

    public partial class SimpleClient : IDisposable
    {

        public HttpClient HttpClient { get; private set; }

        public SimpleClient()
        {
            HttpClient = new HttpClient()
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        public SimpleClient(HttpMessageHandler handler, bool disposeHandler = true)
        {
            HttpClient = new HttpClient(handler, disposeHandler)
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        #region Methods
        public virtual async Task<HttpResponseMessage> GetAsync(int ItemID, int TabID, string GColumnName,string RColumnName, string CColumnName)
        {
            return await HttpClient.GetAsync("api/simple/paged?ItemID=" + ItemID + "&TabID=" + TabID + "&GColumnName=" + GColumnName + "&RColumnName=" + RColumnName + "&CColumnName=" + CColumnName).ConfigureAwait(false);           
        }

        public virtual async Task<HttpResponseMessage> PostAsync(IRItemSearch value)
        {
            return await HttpClient.PostAsJsonAsync<IRItemSearch>("/api/getsimplesearch/paged", value).ConfigureAwait(false);
        }
        #endregion       
        public void Dispose()
        {
            HttpClient.Dispose();
        }
    }

}
namespace ViceroyService
{
    public partial class RequestClient : IDisposable
    {

        public HttpClient HttpClient { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public RequestClient()
        {
            HttpClient = new HttpClient()
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public RequestClient(HttpMessageHandler handler, bool disposeHandler = true)
        {
            HttpClient = new HttpClient(handler, disposeHandler)
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual async Task<HttpResponseMessage> PostAsync(IRRequestSearch search)
        {
            return await HttpClient.PostAsJsonAsync<IRRequestSearch>("api/requestsearch", search).ConfigureAwait(false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual async Task<HttpResponseMessage> GetAsync()
        {
            return await HttpClient.GetAsync("api/Request").ConfigureAwait(false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual async Task<HttpResponseMessage> GetAsync(Int32 id)
        {
            return await HttpClient.GetAsync("api/Request/" + id).ConfigureAwait(false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual async Task<HttpResponseMessage> PostAsync(IRRequest value)
        {
            return await HttpClient.PostAsJsonAsync<IRRequest>("api/Request", value).ConfigureAwait(false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual async Task<HttpResponseMessage> PutAsync(Int32 id, IRRequest value)
        {
            return await HttpClient.PutAsJsonAsync<IRRequest>("api/Request/" + id, value).ConfigureAwait(false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual async Task<HttpResponseMessage> DeleteAsync(Int32 id)
        {
            return await HttpClient.DeleteAsync("api/Request/" + id).ConfigureAwait(false);
        }

        #endregion

        public void Dispose()
        {
            HttpClient.Dispose();
        }
    }

}
namespace ViceroyService
{
    public partial class LabelClient : IDisposable
    {

        public HttpClient HttpClient { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public LabelClient()
        {
            HttpClient = new HttpClient()
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public LabelClient(HttpMessageHandler handler, bool disposeHandler = true)
        {
            HttpClient = new HttpClient(handler, disposeHandler)
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual async Task<HttpResponseMessage> PostAsync(IRLabel value)
        {
            return await HttpClient.PostAsJsonAsync<IRLabel>("api/Label", value).ConfigureAwait(false);
        }

        #endregion

        public void Dispose()
        {
            HttpClient.Dispose();
        }
    }

}
namespace ViceroyService
{
    public partial class InterchangeClient : IDisposable
    {

        public HttpClient HttpClient { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public InterchangeClient()
        {
            HttpClient = new HttpClient()
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public InterchangeClient(HttpMessageHandler handler, bool disposeHandler = true)
        {
            HttpClient = new HttpClient(handler, disposeHandler)
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual async Task<HttpResponseMessage> PutAsync(Int32 id, IRInterchange value)
        {
            return await HttpClient.PutAsJsonAsync<IRInterchange>("api/Interchange/" + id, value).ConfigureAwait(false);
        }

        #endregion

        public void Dispose()
        {
            HttpClient.Dispose();
        }
    }

}
namespace ViceroyService
{
    public partial class TransferClient : IDisposable
    {

        public HttpClient HttpClient { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public TransferClient()
        {
            HttpClient = new HttpClient()
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public TransferClient(HttpMessageHandler handler, bool disposeHandler = true)
        {
            HttpClient = new HttpClient(handler, disposeHandler)
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual async Task<HttpResponseMessage> PostAsync(IRTransfer value)
        {
            return await HttpClient.PostAsJsonAsync<IRTransfer>("api/Transfer", value).ConfigureAwait(false);
        }

        #endregion

        public void Dispose()
        {
            HttpClient.Dispose();
        }
    }

}
namespace ViceroyService
{
    public partial class IRIOClient : IDisposable
    {

        public HttpClient HttpClient { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public IRIOClient()
        {
            HttpClient = new HttpClient()
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public IRIOClient(HttpMessageHandler handler, bool disposeHandler = true)
        {
            HttpClient = new HttpClient(handler, disposeHandler)
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<HttpResponseMessage> PostAsyncInChuncks(IRIO objIO)
        {
            return await HttpClient.PostAsJsonAsync<IRIO>("api/sendChunks", objIO).ConfigureAwait(false);
        }

        #endregion

        public void Dispose()
        {
            HttpClient.Dispose();
        }
    }
}

namespace ViceroyService
{
    public partial class TabClient : IDisposable
    {

        public HttpClient HttpClient { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public TabClient()
        {
            HttpClient = new HttpClient()
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public TabClient(HttpMessageHandler handler, bool disposeHandler = true)
        {
            HttpClient = new HttpClient(handler, disposeHandler)
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual async Task<HttpResponseMessage> GetAsync()
        {
            return await HttpClient.GetAsync("api/Tab").ConfigureAwait(false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual async Task<HttpResponseMessage> GetAsync(Int32 id)
        {
            return await HttpClient.GetAsync("api/Tab/" + id).ConfigureAwait(false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemTypeFilter"></param>
        /// <returns></returns>
        public virtual async Task<HttpResponseMessage> GetAsync(String itemTypeFilter)
        {
            return await HttpClient.GetAsync("api/Tab?itemTypeFilter=" + itemTypeFilter).ConfigureAwait(false);
        }

        #endregion

        public void Dispose()
        {
            HttpClient.Dispose();
        }
    }

}
namespace ViceroyService
{
    public partial class TestClient : IDisposable
    {

        public HttpClient HttpClient { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public TestClient()
        {
            HttpClient = new HttpClient()
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public TestClient(HttpMessageHandler handler, bool disposeHandler = true)
        {
            HttpClient = new HttpClient(handler, disposeHandler)
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual async Task<HttpResponseMessage> GetAsync()
        {
            return await HttpClient.GetAsync("api/Test").ConfigureAwait(false);
        }

        #endregion

        public void Dispose()
        {
            HttpClient.Dispose();
        }
    }

}
namespace ViceroyService
{
    public partial class ServerTimeClient : IDisposable
    {

        public HttpClient HttpClient { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public ServerTimeClient()
        {
            HttpClient = new HttpClient()
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public ServerTimeClient(HttpMessageHandler handler, bool disposeHandler = true)
        {
            HttpClient = new HttpClient(handler, disposeHandler)
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual async Task<HttpResponseMessage> GetAsync()
        {
            return await HttpClient.GetAsync("api/ServerTime").ConfigureAwait(false);
        }

        #endregion

        public void Dispose()
        {
            HttpClient.Dispose();
        }
    }

}
namespace ViceroyService
{
    public partial class PicklistClient : IDisposable
    {

        public HttpClient HttpClient { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public PicklistClient()
        {
            HttpClient = new HttpClient()
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public PicklistClient(HttpMessageHandler handler, bool disposeHandler = true)
        {
            HttpClient = new HttpClient(handler, disposeHandler)
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual async Task<HttpResponseMessage> GetAsync(Int32 id)
        {
            return await HttpClient.GetAsync("api/Picklist/" + id).ConfigureAwait(false);
        }

        #endregion

        public void Dispose()
        {
            HttpClient.Dispose();
        }
    }

}
namespace ViceroyService
{
    public partial class ActionClient : IDisposable
    {

        public HttpClient HttpClient { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public ActionClient()
        {
            HttpClient = new HttpClient()
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public ActionClient(HttpMessageHandler handler, bool disposeHandler = true)
        {
            HttpClient = new HttpClient(handler, disposeHandler)
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual async Task<HttpResponseMessage> GetAsync(Int32 id)
        {
            return await HttpClient.GetAsync("api/Action/" + id).ConfigureAwait(false);
        }

        #endregion

        public void Dispose()
        {
            HttpClient.Dispose();
        }
    }

}
namespace ViceroyService
{
    public partial class DictionaryClient : IDisposable
    {

        public HttpClient HttpClient { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public DictionaryClient()
        {
            HttpClient = new HttpClient()
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public DictionaryClient(HttpMessageHandler handler, bool disposeHandler = true)
        {
            HttpClient = new HttpClient(handler, disposeHandler)
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual async Task<HttpResponseMessage> PostAsync(IRDictionarySearch value)
        {
            return await HttpClient.PostAsJsonAsync<IRDictionarySearch>("api/Dictionary", value).ConfigureAwait(false);
        }

        #endregion

        public void Dispose()
        {
            HttpClient.Dispose();
        }
    }

}
namespace ViceroyService
{
    public partial class MaxItemsOutClient : IDisposable
    {

        public HttpClient HttpClient { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public MaxItemsOutClient()
        {
            HttpClient = new HttpClient()
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public MaxItemsOutClient(HttpMessageHandler handler, bool disposeHandler = true)
        {
            HttpClient = new HttpClient(handler, disposeHandler)
            {
                BaseAddress = new Uri(DatabaseServices.InfolinxRestBaseAddress)
            };
        }

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual async Task<HttpResponseMessage> GetAsync(int userId, int containedTabId)
        {
            return await HttpClient.GetAsync("api/MaxItemsOut?userID=" + userId + "&containedTabID=" + containedTabId).ConfigureAwait(false);
        }

        #endregion

        public void Dispose()
        {
            HttpClient.Dispose();
        }
    }

}

