﻿using System;
using System.IO;

namespace ViceroyService
{
    public class Logger
    {
        private string FullFileName { get; set; }

        public Logger()
        {
            SetUpLogFile();
        }

        private void SetUpLogFile()
        {
            string path = DatabaseServices.LogFilePath;
            string message = "";


            if (!Directory.Exists(path))
            {
                message = "Log file path changed from " + path + " to ";

                path = Path.Combine(Environment.CurrentDirectory, "log");

                Directory.CreateDirectory(path);

                DatabaseServices.LogFilePath = path;

                DatabaseServices ds = new DatabaseServices(Properties.Settings.Default.ConnectionString);
                ds.UpdateVRSettings("LogFilePath", "System.String", "User", path);

                // Properties.Settings.Default.Save();

                message += path;
            }

            string name = "VR_Service" + DateTime.Now.ToFileTime() + ".log";
            FullFileName = Path.Combine(path, name);

            if (message != String.Empty)
            {
                this.LogMessageToFile(message);
            }
        }

        public void LogMessageToFile(string msg)
        {
            msg = string.Format("{0:G}: {1}{2}", DateTime.Now, msg, Environment.NewLine);
            File.AppendAllText(FullFileName, msg);
        }

        public void LogException(string source, Exception ex)
        {
            string detailedErrorMessage = ex.ToString();

            while (ex.InnerException != null)
            {
                detailedErrorMessage += $" {ex.InnerException.ToString()}";

                ex = ex.InnerException;
            }

            this.LogMessageToFile(source + " " + detailedErrorMessage);
        }
    }
}
