﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ViceroyService
{
    public class SuperSynchronizer
    {
        private DirectoryInfo[] _clientFolders;

        private List<IRSimpleItem> _infolinxMatters;

        private Dictionary<string, string> _clientMap;

        public async Task PublicallyAvailableMethodThatDoesEverythingAsync()
        {
            string path = @"C:\AirTravel\ok";
            GetClientFoldersForDirectory(path);
            await MatchInfolinxClientsToClientFoldersAsync("%");

        }

        public async Task MatchInfolinxClientsToClientFoldersAsync(string searchPattern)
        {
            Dictionary<string, string> clientMap = new Dictionary<string, string>();

            // Somehow get a collection of all the clients in Infolinx
            IEnumerable<string> infolinxClients = await GetInfolinxClients();

            foreach (var client in infolinxClients)
            {
                // Get all the client folders that match this client according to the search pattern.
                // Then add a dictionary entry for each one.
                IEnumerable<DirectoryInfo> clientFolderMatches = _clientFolders.Where(directory => 
                    directory.Name == searchPattern.Replace("%", client).Replace("*", @"[\w\-. ]+"));

                foreach (var match in clientFolderMatches)
                {
                    clientMap.Add(client, match.FullName);
                }
            }

            _clientMap = clientMap;
        }

        private async Task<IEnumerable<string>> GetInfolinxClients()
        {
            _infolinxMatters = await new InfolinxItem().GetAllMatters(-1);
            IEnumerable<string> infolinxClients = _infolinxMatters.Select(matter => matter.R);
            return infolinxClients;
        }

        public void GetClientFoldersForDirectory(string path)
        {
            if (!Directory.Exists(path))
                throw new ArgumentException("The specified directory does not exist.");

            _clientFolders = new DirectoryInfo(path).GetDirectories();
        }
    }
}
